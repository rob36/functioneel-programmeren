Rob Stortelder    S1279114
Antoine Veenstra  S1378791
--------------------------


Om de compiler te kunnen gebruiken moet op de commandline de "ghci compiler.hs" aangeroepen worden en moeten de bestanden Exec.hs, TypesEtc.hs Sprockell.hs aanwezig zijn in dezelfde folder als compiler.hs. 

Voorbeeld Commando's die uitgevoerd kunnen worden staan hieronder aangegeven. Om zelf een programma uit te voeren is het volgende commando nodig:
sim ([registers],[memory adressen]) (compile [programmacode]) 

Het verslag bevat de werking van de compiler en de syntax. Voorbeelden staan onderaan de readme beschreven.

----voorbeeldprogramma's-------
Fibonacci
sim ([],[42]) (compile "x=20; n2=1; for(x=x-1; x; x=x-1){ temp = n1 +n2; n1 =n2; n2 = temp;}")
Priemgetal
sim ([],[44]) (compile "x=7;n=2;while( x != n && x % n != 0){n=n+1;}ans = x==n;")
Systeemtest
sim ([],[46]) (compile "a=10;for(i=0;i<10;i=i+1){if(a>5){a=a-1;}}b=true;c=7;while(b){if(c>0){c=c-2;} else {b = false;}}d = 10 / 5 + 2 * 3 - 1;ans = a == 5 && (!b) && c == -1 && d == 7;")

