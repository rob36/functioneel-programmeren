import Prelude
import Data.Maybe
import Data.Char
import Data.List
import Sprockell
import TypesEtc
import Exec

-- The tokenizer outputs a list of these.
data Token  = Tif
        | Telse
        | Twhile
        | Tfor
        | Tlbrace
        | Trbrace
        | Tlparen
        | Trparen
        | Tsemic
        | Tdiv
        | Tmul
        | Tsub
        | Tadd
        | Tmod
        | Tassign
        | Tband
        | Tbor
        | Tnot
        | Teq
        | Tge
        | Tse
        | Tne
        | Tgt
        | Tst
        | Tfalse
        | Ttrue
        | Tconstant Int
        | Tvariable [Char]
        | Texpr Tree 
         deriving (Eq, Show)

-- Reserved names in our language.
reserved :: [([Char],Token)]
reserved = [("if",Tif),("else",Telse),("while",Twhile),("for",Tfor),("true",Ttrue),("false",Tfalse)]

-- Returns the token matching the name if it is in the reserved tokens list.
getReserved :: [Char] -> Maybe Token
getReserved name = getReserved' name reserved

getReserved' :: [Char] -> [([Char],Token)] -> Maybe Token
getReserved' name [] = Nothing
getReserved' name ((n,t):xs)
    | name == n = Just t
    | otherwise = getReserved' name xs

-- Tokenizes the given input
tokenizer :: [Char] -> [Token]
tokenizer [] = []

tokenizer (' ':xs)  = tokenizer xs
tokenizer ('\n':xs) = tokenizer xs
tokenizer ('\r':xs) = tokenizer xs
tokenizer ('\t':xs) = tokenizer xs

tokenizer ('{':xs)  = Tlbrace   : (tokenizer xs)
tokenizer ('}':xs)  = Trbrace   : (tokenizer xs)

tokenizer ('(':xs)  = Tlparen   : (tokenizer xs)
tokenizer (')':xs)  = Trparen   : (tokenizer xs)

tokenizer (';':xs)  = Tsemic    : (tokenizer xs)

tokenizer ('/':xs)  = Tdiv      : (tokenizer xs)
tokenizer ('*':xs)  = Tmul      : (tokenizer xs)

tokenizer ('-':xs)  = Tsub      : (tokenizer xs)
tokenizer ('+':xs)  = Tadd      : (tokenizer xs)

tokenizer ('%':xs)  = Tmod      : (tokenizer xs)

tokenizer ('=':'=':xs)  = Teq   : (tokenizer xs)
tokenizer ('>':'=':xs)  = Tge   : (tokenizer xs)
tokenizer ('<':'=':xs)  = Tse   : (tokenizer xs)
tokenizer ('!':'=':xs)  = Tne   : (tokenizer xs)

tokenizer ('=':xs)  = Tassign   : (tokenizer xs)

tokenizer ('&':'&':xs)  = Tband : (tokenizer xs)
tokenizer ('|':'|':xs)  = Tbor  : (tokenizer xs)
tokenizer ('!':xs)      = Tnot  : (tokenizer xs)

tokenizer ('>':xs)  = Tgt       : (tokenizer xs)
tokenizer ('<':xs)  = Tst       : (tokenizer xs)
tokenizer (x:xs)
    | elem x ['0'..'9'] = (Tconstant c):(tokenizer rest1)
    | elem x (['A'..'Z']++['a'..'z']) = t:(tokenizer rest2)
    | otherwise = error ("Unknown input \""++[x]++"\" on a line.")
    where
        (c,rest1) = getConstant 0 (x:xs)
        (t,rest2) = getVariable (x:xs)


-- Get the next variable or one of the reserved tokens.
getVariable :: [Char] -> (Token,[Char])
getVariable list
    | isJust token = (fromJust token,rest)
    | otherwise = (Tvariable name,rest)
    where
        (name,rest) = getVariable' list
        token = getReserved name

getVariable' :: [Char] -> ([Char],[Char])
getVariable' [] = ([],[])
getVariable' (x:xs)
    | elem x (['A'..'Z']++['a'..'z']++['1'..'9']) = (x:(var),rest)
    | otherwise = ([],x:xs)
    where
        (var,rest) = getVariable' xs


-- Get the next constant (that's a number)
getConstant :: Int -> [Char] -> (Int, [Char])
getConstant val [] = (val, [])
getConstant val (x:xs)
    | elem x ['0' .. '9'] = getConstant ((val*10)+(ord x)-(ord '0')) xs
    | otherwise = (val, (x:xs))


-- The tree containging an expression
data Tree   = Leaf Token
            | Node Token Tree Tree
            | Nif Tree [Tree] [Tree]
            | Nwhile Tree [Tree]
            | Nfor Tree Tree Tree [Tree]
            | Nnot Tree
            | Nneg Tree
            deriving (Eq, Show)


-- Grows a tree (an expression)
growTree :: [Token] -> Tree
growTree ts = growTree' $ deparen ts

growTree' :: [Token] -> Tree
growTree' [] = error "Geen tekst is geen functie..."
growTree' [x@(Tvariable _)] = Leaf x
growTree' (x@(Tvariable _):y@(Tassign):xs) = Node y (Leaf x) (growTree' xs)
growTree' [x@(Tconstant _)] = Leaf x
growTree' [x@(Ttrue)] = Leaf (Tconstant 1)
growTree' [x@(Tfalse)] = Leaf (Tconstant 0)
growTree' (Tsub:xs) = Nneg (growTree' xs)
growTree' [x@(Texpr t)] = t
growTree' ts
    | isJust x1 = Node (fromJust x1) (growTree' l1) (growTree' r1)
    | isJust x2 = Node (fromJust x2) (growTree' l2) (growTree' r2)
    | isJust x3 = growTree' (l3 ++ [Texpr (Nnot (growTree' r3))])
    | isJust x4 = Node (fromJust x4) (growTree' l4) (growTree' r4)
    | isJust x5 = Node (fromJust x5) (growTree' l5) (growTree' r5)
    | otherwise = error ("Fix je operatoren, jij faalhaas! \"" ++ (show ts)++"\"")
    where
        (l1, x1, r1) = getTillNext [] ts [Tband, Tbor]
        (l2, x2, r2) = getTillNext [] ts [Teq, Tge, Tse, Tne, Tgt, Tst]
        (l3, x3, r3) = getTillNext [] ts [Tnot]
        (l4, x4, r4) = getTillNext [] ts [Tadd, Tsub]
        (l5, x5, r5) = getTillNext [] ts [Tdiv, Tmul, Tmod]


-- Grows a forest (a list of expressions)
growForest :: [Token] -> [Tree]
growForest [] = []


-- Grow an if statement with a condition, den, and els. There is no explicit check for parenthesis around the condition, so no error will be thrown.
growForest (x@(Tif):ts)
    | isJust lbrace = [Nif cond' (growForest den) (growForest els)] ++ (growForest r'')
    | otherwise = error "De if statement moet een linker accolade hebben, jij landrot!"
    where
        (cond,lbrace,r) = getTillNext [] ts [Tlbrace]
        cond' = growTree cond
        (den,r') = getMatchingBrace r 1
        (els,r'') = getElse r'


-- Grow a while statement with a condition, and els.
growForest (x@(Twhile):ts)
    | isJust lbrace = [Nwhile cond' (growForest den)] ++ (growForest r')
    | otherwise = error "De while statement moet een linker accolade hebben, jij landrot!"
    where
        (cond,lbrace,r) = getTillNext [] ts [Tlbrace]
        cond' = growTree cond
        (den,r') = getMatchingBrace r 1


-- Grow a for statement with an ent, condition, step, and den.
growForest (x@(Tfor):ts)
    | isJust lbrace && isJust semic1 && isJust semic2 && (head ent) == Tlparen && (last step) == Trparen = [Nfor (growTree (tail ent)) (growTree cond) (growTree (init step)) (growForest den)] ++ (growForest r')
    | otherwise = error "De for statement is niet goed gevormd, jij tuig!"
    where
        (fstat,lbrace,r) = getTillNext [] ts [Tlbrace]
        (ent,semic1,fstat') = getTillNext [] fstat [Tsemic]
        (cond,semic2,step) = getTillNext [] fstat' [Tsemic]
        (den,r') = getMatchingBrace r 1


-- Grow a tree for the expression until the semicolon
growForest ts
    | isJust x && l /= [] = [growTree l] ++ growForest r
    | otherwise = error "Fix je puntkomma's, aarshol!"
    where
        (l, x, r) = getTillNext [] ts [Tsemic]  


-- Get the else statement if there is one. If the next token is not Telse an empty list is returned indicating there is no else.
getElse :: [Token] -> ([Token],[Token])
getElse (Telse:Tlbrace:ts) = getMatchingBrace ts 1
getElse (Telse:ts) = error "De else moet een accolade hebben! Schavuit!"
getElse ts = ([],ts)


-- Removes the parentheses from the token list and replaces the expression between them by Texp with a Tree containing the expression.
deparen :: [Token] -> [Token]
deparen ts
    | isJust d = deparen $ fromJust d
    | otherwise = ts
    where
        d = deparen' [] [] ts

deparen' :: [Token] -> [Token] -> [Token] -> Maybe [Token]
deparen' ls lp ts
    | isJust x && (fromJust x) == Tlparen = Just (ls ++ (fromJust $ deparen' (lp++left) [fromJust x] right))
    | isJust x && lp /= [] && (fromJust x) == Trparen = Just (ls ++ [Texpr (growTree left)] ++ right)
    | isJust x && (fromJust x) == Trparen = error "Focking rechter haakje te veel motherfocker!!1!!"
    | lp /= [] && x == Nothing = error "Focking linker haakje te veel motherfocker!!1!!"
    | otherwise = Nothing
    where
        (left,x,right) = getTillNext [] ts [Tlparen,Trparen]


-- Returns the matching brace inside a list of tokens.
getMatchingBrace :: [Token] -> Int -> ([Token],[Token])
getMatchingBrace [] _ = error "Focking rechter accolade te weinig!! You bastard!"
getMatchingBrace (x@(Trbrace):ts) 1 = ([],ts)
getMatchingBrace (x@(Trbrace):ts) depth = (x:l,r)
    where
        (l,r) = getMatchingBrace ts (depth-1)

getMatchingBrace (x@(Tlbrace):ts) depth = (x:l,r)
    where
        (l,r) = getMatchingBrace ts (depth+1)

getMatchingBrace (x:ts) d = (x:l,r)
    where
        (l,r) = getMatchingBrace ts d


-- Returns the first occurrence of one of the tokens in the given list.
getTillNext :: [Token] -> [Token] -> [Token] -> ([Token],Maybe Token,[Token])
getTillNext left [] tofind = (left,Nothing,[])
getTillNext left (x:right) tofind
    | getTillNext' x tofind = (left,Just x,right)
    | otherwise = getTillNext (left++[x]) right tofind

getTillNext' :: Token -> [Token] -> Bool
getTillNext' t [] = False
getTillNext' t (x:xs) = t == x || getTillNext' t xs



type Mem = Int
type Reg = Int
type Vars = [(Token,Mem)]

-- Returns the Token of one element of Vars.
getVar :: (Token,Mem) -> Token
getVar (v,_) = v

-- Returns the memory location of one element of Vars.
getMem :: (Token,Mem) -> Mem
getMem (_,m) = m

-- Contains the starting memory location of our program.
startMem :: Int
startMem = sp0+22

-- Makes the Vars for the program.
makeVarlist :: [Token] -> Vars
makeVarlist ts = vs
    where
        (vs,_) = makeVarlist' ts

makeVarlist' :: [Token] -> (Vars,Mem)
makeVarlist' [] = ([],startMem-1)
makeVarlist' (x@(Tvariable _):xs)
    | found = ((x,m'):vs,m')
    | otherwise = (vs,m)
    where
        (vs,m) = makeVarlist' xs
        found = isNothing $ find (\q -> (getVar q) == x) vs
        m' = m+1

makeVarlist' (x:xs) = makeVarlist' xs

-- Returns the variable with the given name
findVariable :: Token -> Vars -> (Token,Mem)
findVariable v vs = fromJust $ find (\q -> (getVar q) == v) vs

-- Compiles an expression to Assembly.
compileE :: Tree -> Vars -> [Assembly]
compileE t vs = compileE' t vs jmpreg

-- Compiles an expression to Assembly and makes sure the result is put in right place.
compileE' :: Tree -> Vars -> Reg -> [Assembly]
compileE' (Node Tassign (Leaf v@(Tvariable _)) t2) vs r = (compileE' t2 vs r')++[Store (Addr r') m]++after
    where
        m = getMem $ findVariable v vs
        (r',after) = getRegSingle r vs

compileE' (Leaf (Tconstant c)) vs r = [Load (Imm c) r']++after
    where
        (r',after) = getRegSingle r vs

compileE' (Leaf v@(Tvariable _)) vs r = [Load (Addr m) r']++after
    where
        m = getMem $ findVariable v vs
        (r',after) = getRegSingle r vs
        
compileE' (Nnot t1) vs r = (compileE' t1 vs r')++[Compute Equal r' 0 r']++after
    where
        (r',after) = getRegSingle r vs

compileE' (Nneg t1) vs r = (compileE' t1 vs r')++[Compute Neg r' 0 r']++after
    where
        (r',after) = getRegSingle r vs

compileE' (Node Tadd t1 t2) vs r = (compileE' t1 vs' v1)++(compileE' t2 vs' v2)++before++[Compute Add r1 r2 out]++after
    where
        (v1,v2,before,r1,r2,out,after,vs') = getRegDouble r vs

compileE' (Node Tsub t1 t2) vs r = (compileE' t1 vs' v1)++(compileE' t2 vs' v2)++before++[Compute Sub r1 r2 out]++after
    where
        (v1,v2,before,r1,r2,out,after,vs') = getRegDouble r vs

compileE' (Node Tmul t1 t2) vs r = (compileE' t1 vs' v1)++(compileE' t2 vs' v2)++before++[Compute Mul r1 r2 out]++after
    where
        (v1,v2,before,r1,r2,out,after,vs') = getRegDouble r vs

compileE' (Node Tdiv t1 t2) vs r = (compileE' t1 vs' v1)++(compileE' t2 vs' v2)++before++[Compute Div r1 r2 out]++after
    where
        (v1,v2,before,r1,r2,out,after,vs') = getRegDouble r vs

compileE' (Node Tmod t1 t2) vs r = (compileE' t1 vs' v1)++(compileE' t2 vs' v2)++before++[Compute Mod r1 r2 out]++after
    where
        (v1,v2,before,r1,r2,out,after,vs') = getRegDouble r vs


compileE' (Node Teq t1 t2) vs r = (compileE' t1 vs' v1)++(compileE' t2 vs' v2)++before++[Compute Equal r1 r2 out]++after
    where
        (v1,v2,before,r1,r2,out,after,vs') = getRegDouble r vs

compileE' (Node Tge t1 t2) vs r = (compileE' t1 vs' v1)++(compileE' t2 vs' v2)++before++[Compute Incr r1 0 r1, Compute Gt r1 r2 out]++after
    where
        (v1,v2,before,r1,r2,out,after,vs') = getRegDouble r vs

compileE' (Node Tse t1 t2) vs r = (compileE' t1 vs' v1)++(compileE' t2 vs' v2)++before++[Compute Incr r2 0 r2, Compute Lt r1 r2 out]++after
    where
        (v1,v2,before,r1,r2,out,after,vs') = getRegDouble r vs

compileE' (Node Tne t1 t2) vs r = (compileE' t1 vs' v1)++(compileE' t2 vs' v2)++before++[Compute NEq r1 r2 out]++after
    where
        (v1,v2,before,r1,r2,out,after,vs') = getRegDouble r vs

compileE' (Node Tgt t1 t2) vs r = (compileE' t1 vs' v1)++(compileE' t2 vs' v2)++before++[Compute Gt r1 r2 out]++after
    where
        (v1,v2,before,r1,r2,out,after,vs') = getRegDouble r vs

compileE' (Node Tst t1 t2) vs r = (compileE' t1 vs' v1)++(compileE' t2 vs' v2)++before++[Compute Lt r1 r2 out]++after
    where
        (v1,v2,before,r1,r2,out,after,vs') = getRegDouble r vs

compileE' (Node Tband t1 t2) vs r = (compileE' t1 vs' v1)++(compileE' t2 vs' v2)++before++[Compute And r1 r2 out]++after
    where
        (v1,v2,before,r1,r2,out,after,vs') = getRegDouble r vs

compileE' (Node Tbor t1 t2) vs r = (compileE' t1 vs' v1)++(compileE' t2 vs' v2)++before++[Compute Or r1 r2 out]++after
    where
        (v1,v2,before,r1,r2,out,after,vs') = getRegDouble r vs


compileE' x vs r = error ("This can not be: \""++(show x)++"\". Please contact your system administrator.")


-- Returns the required values and commands to ensure the input and output is in the right place before and after execution of the command.
getRegDouble :: Reg -> Vars -> (Reg, Reg, [Assembly], Reg, Reg, Reg, [Assembly],Vars)
getRegDouble r vs
    | r < rg1 = (r,r+1,[],r,r+1,r,[],vs)
    | r == rg1 = (ms,rg1,[Load (Addr ms) rg2],rg2,r,r,[],vs')
    | otherwise = (r,rg1,[Load (Addr r) rg2],rg2,rg1,rg2,[Store (Addr rg2) r],vs')
    where
        ms = startMem + (length vs)
        rg1 = regbanksize - 2
        rg2 = regbanksize - 1
        vs' = (Tnot,ms):vs

-- Returns the required values and commands to ensure unary commands return the output in the right place.
getRegSingle :: Reg -> Vars -> (Reg,[Assembly])
getRegSingle r vs
    | r <= rg1 = (r,[])
    | otherwise = (rg1,[Store (Addr rg1) r])
    where
        ms = startMem + (length vs)
        rg1 = regbanksize - 2
        rg2 = regbanksize - 1

-- Compiles statements. If, For, and While are handeled seperatly. Expressions are compiled with compileE.
compileP :: [Tree] -> Vars -> [Assembly]
compileP [] vs = []
compileP (t@(Nif cond den []):xs) vs = compcond ++ [Jump CR ((length compden)+1)] ++ compden ++ (compileP xs vs)
    where
        compcond = (compileE' cond vs 1) ++ [Compute Equal 1 0 1]
        compden = compileP den vs

compileP (t@(Nif cond den els):xs) vs = compcond ++ [Jump CR ((length compden)+1)] ++ compden ++ compels ++ (compileP xs vs)
    where
        compcond = (compileE' cond vs 1) ++ [Compute Equal 1 0 1]
        compden = (compileP den vs) ++ [Jump UR ((length compels)+1)]
        compels = compileP els vs


compileP (t@(Nwhile cond den):xs) vs = compcond ++ [Jump CR ((length compden)+1)] ++ compden ++ (compileP xs vs)
    where
        compcond = (compileE' cond vs 1) ++ [Compute Equal 1 0 1]
        compden' = compileP den vs
        compden = compden' ++ [Jump UR (-(length compcond)-(length compden))]


compileP (t@(Nfor ent cond step den):xs) vs = compent ++ compcond ++ [Jump CR ((length compden)+(length compstep)+1)] ++ compstep ++ compden ++ (compileP xs vs)
    where
        compent = (compileE ent vs)
        compcond = (compileE' cond vs 1) ++ [Compute Equal 1 0 1]
        compstep = (compileE step vs)
        compden' = compileP den vs
        compden = compden' ++ [Jump UR (-(length compcond)-(length compden)-(length compstep))]


compileP (t:ts) vs = (compileE t vs) ++ (compileP ts vs)

-- Compiles a string to an Assembly program.
compile :: [Char] -> [Assembly]
compile text = (compileP forest vars) ++ [EndProg]
    where
        tokens = tokenizer text
        vars = makeVarlist tokens
        forest = growForest tokens


