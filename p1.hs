import FPPrac

-- opgave 1
f x = 2*x^2+3*x-5

-- opgave 2
codeer :: Number -> Char -> Char
codeer n x | 'a' <= x && x <= 'z' = chr( ord 'a' + ( xn - ord 'a' ) `mod` 26 )
	   | 'A' <= x && x <= 'Z' = chr( ord 'A' + ( xn - ord 'A' ) `mod` 26 )
           | otherwise = x
	where 
          xn = ord x + n


-- opgave 3
-- rente :: (num, num, num) -> num 
rente (n,b,r) | n < 1 = b
              | n == 1 = b*r  
              | otherwise = rente(n-1,b*r,r)

-- opgave 4
-- discr :: num -> num -> num -> num 
-- wortel1 :: num -> num -> num -> num 
-- wortel2 :: num -> num -> num -> num 
discr a b c = b^2-4*a*c
wortel1 a b c | d < 0 = error "Discriminant < 0"
	      | otherwise = (-b + sqrt(d))/(2*a)
	      where
                d = discr a b c

wortel2 a b c | d < 0 = error "Discriminant < 0"
	      | otherwise = (-b - sqrt(d))/(2*a)
	      where
                d = discr a b c

--opgave 5
-- extrX :: num -> num -> num -> num 
-- extrY :: num -> num -> num -> num 
extrX a b c = (wortel1 a b c + wortel2 a b c)/2
extrY a b c = a*x^2+b*x+c
         where x = extrX a b c

--opgave 6
-- mylength :: [t] -> num
mylength [] = 0
mylength (x:xs) = 1 + mylength xs

-- mysum :: [t] -> num
mysum [] = 0
mysum (x:xs) = x + mysum xs

-- myreverse :: [t] -> [t]
myreverse [] = []
myreverse (x:xs) = myreverse xs ++ [x]

-- mytake :: [t] -> [t]
mytake [] n = []
mytake (xs) 0 = []
mytake (x:xs) n = x : mytake xs (n-1)  

-- myelem :: [t] -> bool
myelem [] n = False
myelem (x:xs) n = x==n || myelem xs n

-- myconcat :: [t] -> [t]
myconcat [] = []
myconcat (x:xs) = x ++ myconcat xs

-- mymaximum :: [t] -> num
mymaximum [] = error "Lege lijst!"
mymaximum [x] = x
mymaximum (x:xs) = max x (mymaximum xs)

-- mytake :: [a] -> [b] -> [(a,b)]
myzip (xs) [] = []
myzip [] (xs) = []
myzip (x:xs) (y:ys) = (x,y) : myzip xs ys

--opgave 7
-- num -> num -> [num]
r s v = [s] ++ r (s+v) v
-- num -> num -> num -> num
r1 s v n = (r s v) !! n
-- num -> num -> num -> num -> num
totaal i j s v = mysum (drop i (take j (r s v)))  

--opgave 8
-- [t] -> bool
allEqual [] = True
allEqual [x] = True
allEqual (x:y:xs) = x == y && allEqual(y:xs) 

-- [num] -> [num]
diff [] = []
diff [x] = []
diff (x:y:xs) = [x-y] ++ diff(y:xs) 

-- [num] -> bool
isRR l = allEqual (diff l)

--opgave 9
--a
allEqualLength x = allEqual (map mylength x)
--b
allTotals x = map mysum x
--d

transponeer [] = []
transponeer x	| allEqualLength x = transponeer' x
		| otherwise = error "Niet zelfde lengte"
transponeer' x	| 1 < l = [ map head x  ] ++ transponeer (map tail x)
		| l == 1 = [ map head x  ]
		| otherwise = []
		where
			l = mylength (head x)

-- c
colomnTotals x = map mysum (transponeer x)
		
			






























