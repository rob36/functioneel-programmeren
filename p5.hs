import FPPrac
import FPPrac.Trees.RedBlackTree

data Col = R | B | G -- Red, Black, Grey
data RBTree = T Col NodeType
data NodeType = L | N Number RBTree RBTree

aftekenBoom = T B (N 15
                        (T B (N 7
                            (T R (N 3
                                (T B (N 1
                                    (T R (N 0
                                        (T B L)
                                        (T B L)
                                    ))
                                    (T R (N 2
                                        (T B L)
                                        (T B L)
                                    ))
                                ))
                                (T B (N 5
                                    (T R (N 4
                                        (T B L)
                                        (T B L)
                                    ))
                                    (T R (N 6
                                        (T B L)
                                        (T B L)
                                    ))
                                ))
                            ))
                            (T B (N 10
                                (T R (N 8
                                    (T B L)
                                    (T B L)
                                ))
                                (T R (N 12
                                    (T B L)
                                    (T B L)
                                ))
                            ))
                        ))
                        (T B (N 25
                            (T B (N 20
                                (T B L)
                                (T B L)
                            ))
                            (T R (N 30
                                (T B (N 28
                                    (T B L)
                                    (T B L)
                                ))
                                (T B (N 60
                                    (T B L)
                                    (T B L)
                                ))
                            ))
                        ))
                    )
                    
-- vooraf
rbZetom :: RBTree -> RBTreeG
rbZetom (T a (L)) = RBNodeG (zetomCol a) "" []
rbZetom (T a (N n t1 t2)) = RBNodeG (zetomCol a) (show n) [rbZetom t1,rbZetom t2]

zetomCol :: Col -> ColorG
zetomCol R = RedG
zetomCol B = BlackG
zetomCol G = GreyG

--Insertions
-- 1
insert :: Number -> RBTree -> RBTree
insert num (T B L) = T R (N num (T B L) (T B L))
insert num (T a (N x t1 t2))	| num < x = T a (N x (insert num t1) t2)
								| num > x = T a (N x t1 (insert num t2))
								| otherwise = error "already in the tree"

-- 2
rootToBlack (T _ a) = T B a

-- 3
--colourFlip (T _ (N num (T _ a) (T _ b))) = T R (N num (T B a) (T B b))
colourFlip (T B (N n (T R (N n2 (T R x) t2)) (T R b))) = (T R (N n (T B (N n2 (T R x) t2)) (T B b)))
colourFlip (T B (N n (T R (N n2 t2 (T R x))) (T R b))) = (T R (N n (T B (N n2 t2 (T R x))) (T B b)))
colourFlip (T B (N n (T R b) (T R (N n2 (T R x) t2)))) = (T R (N n (T B b) (T B (N n2 (T R x) t2))))
colourFlip (T B (N n (T R b) (T R (N n2 t2 (T R x))))) = (T R (N n (T B b) (T B (N n2 t2 (T R x)))))
colourFlip (T x y) = (T x y)

-- 4
rebalance (
	T B (
		N num
			(T R
				(N knum
					(T R (N kknum (T clll tlll) (T cllr tllr)))
					(T clr tlr)
				)
			)
			(T cr tr)
	)) = T B (N knum (T R (N kknum (T clll tlll) (T cllr tllr))) (T R (N num (T clr tlr) (T cr tr))))
rebalance (
	T B (
		N num
			(T R
				(N knum
					(T cll tll)
					(T R (N kknum (T clrl tlrl) (T clrr tlrr)))
				)
			)
			(T cr tr)
		)
	) = T B (N kknum (T R (N knum (T cll tll) (T clrl tlrl))) (T R (N num (T clrr tlrr) (T cr tr))))
rebalance (
	T B
		(N num
			(T cl tl)
			(T R
				(N knum
					(T R (N kknum (T crll trll) (T crlr trlr)))
					(T crr trr)
				)
			)
		)
	) = T B (N kknum (T R (N num (T cl tl) (T crll trll))) (T R (N knum (T crlr trlr) (T crr trr))))
rebalance (
	T B (
		N num
			(T cl tl)
			(T R (
				N knum
					(T crl trl)
					(T R (
						N kknum
							(T crrl trrl)
							(T crrr trrr)
						)
					)
				)
			)
		)
	) = T B (N knum (T R (N num (T cl tl) (T crl trl))) (T R (N kknum (T crrl trrl) (T crrr trrr))))
rebalance (T a (N num b c)) = (T a (N num b c))
rebalance (T x L) = (T x L)


--5
balancedInsert num tree = rootToBlack $ balancedInsert' $ insert num tree
balancedInsert' (T B L) = (T B L)
balancedInsert' (T x (N y t1 t2)) = rebalance $ colourFlip (T x (N y (balancedInsert' t1) (balancedInsert' t2)))

{-
balancedInsert :: Number -> RBTree -> RBTree
balancedInsert num tree = rootToBlack result
				where
					(result,_,_) = balancedInsert' (insert num tree)

balancedInsert' (T a L) = ((T a L),0,-1)
balancedInsert' (T R (N x t1 t2))	| (min rb1 rb2) == -1 = ((T R (N x tree1 tree2)), (max cf1 cf2) +1, ((max rb1 rb2) +1))
									| otherwise = ((T R (N x tree1 tree2)), (max cf1 cf2) +1, -2)
									where
										(tree1, cf1,rb1) = balancedInsert' t1
										(tree2, cf2,rb2) = balancedInsert' t2

balancedInsert' (T B (N x t1 t2))	| (max cf1 cf2) >= 2 && (min cf1 cf2) >= 1 = (colourFlip (T B (N x tree1 tree2)),0, 0)
									| (min rb1 rb2) == -1 && (max rb1 rb2) ==1 = (rebalance (T B (N x tree1 tree2)),0, 0)
									| otherwise = (T B (N x tree1 tree2),0, 0)
									where
										(tree1, cf1,rb1) = balancedInsert' t1
										(tree2, cf2,rb2) = balancedInsert' t2                                 
-}

--Deletions
--1
leftmostValue (T a (N x (T b L) t2)) 			= x
leftmostValue (T _ (N x (T a (N b t1 c)) t2)) 	= leftmostValue (T a (N b t1 c))

--2
-- Als de node zelf zwart is en de root van zijn rechter subboom is rood, dan is de enige mogelijkheid dat deze weer twee leaves heeft om de rood-zwart eigenschap in stand te houden.

--3
removeLeftmostNode (T B (N b (T B L) (T B L))) = T G L
removeLeftmostNode (T B (N b (T B L) (T R x))) = T B x 
removeLeftmostNode (T R (N b (T B L) (T B L))) = T B L
removeLeftmostNode (T a (N b (T R (N _ (T B L) _)) t2)) = (T a (N b (T B L) t2))
removeLeftmostNode (T a (N b (T B (N _ (T B L) (T B L))) t2)) = (T a (N b (T G L) t2))
removeLeftmostNode (T a (N b (T B (N _ (T B L) (T _ d))) t2)) = (T a (N b (T B d) t2))
removeLeftmostNode (T a (N b t1 t2)) = (T a (N b (removeLeftmostNode t1) t2))


--4
--a
greyColourFlip (T B (N p (T G g) (T B (N s (T B l ) (T B r))))) 					= (T G (N p (T B g) (T R (N s (T B l ) (T B r)))))
greyColourFlip (T B (N p (T B (N s (T B l ) (T B r))) (T G g)))						= (T G (N p (T R (N s (T B r) (T B l ))) (T B g)))
--b
greyColourFlip (T x (N p (T G g) (T B (N s (T R (N l (T B a ) (T B b))) (T y r))))) = (T x (N l (T B (N p (T B g) (T B a))) (T B (N s (T B b) (T y r)))))
greyColourFlip (T x (N p (T B (N s (T y r) (T R (N l (T B b) (T B a ))))) (T G g))) = (T x (N l (T B (N s (T y r) (T B b))) (T B (N p (T B a) (T B g)))))
--c
greyColourFlip (T R (N p (T G g) (T B (N s (T B l ) (T x r))))) 					= (T B (N s (T R (N p (T B g) (T B l ))) (T x r)))
greyColourFlip (T R (N p (T B (N s (T x r) (T B l ))) (T G g))) 					= (T B (N s (T x r) (T R (N p (T B l ) (T B g)))))
--d
greyColourFlip (T B (N p (T G g) (T B (N s (T B l ) (T R r))))) 					= (T B (N s (T B (N p (T B g) (T B l ))) (T B r)))
greyColourFlip (T B (N p (T B (N s (T R r) (T B l ))) (T G g))) 					= (T B (N s (T B r) (T B (N p (T B l ) (T B g)))))
--e
greyColourFlip (T B (N p (T G g) (T R (N s (T B l ) (T B r))))) 					= (T B (N s (T R (N p (T G g) (T B l ))) (T B r)))
greyColourFlip (T B (N p (T R (N s (T B r) (T B l ))) (T G g))) 					= (T B (N s (T B r) (T R (N p (T B l ) (T G g)))))
--other
greyColourFlip (T c1 t1) = (T c1 t1)


--5
greyRebalance (T c L) = (T c L)
greyRebalance (T c (N n t1 t2)) = greyColourFlip (T c (N n (greyRebalance t1) (greyRebalance t2)))
--greyRebalance (T c1 (N p (T G x) t2)) = greyColourFlip (T c1 (N p (T G x) t2))
--greyRebalance (T c1 (N p t2 (T G x))) = greyColourFlip (T c1 (N p t2 (T G x)))
--greyRebalance (T c1 (N p t1 t2)) = (T c1 (N p (greyRebalance t1)  (greyRebalance t2)))
--greyRebalance (T c1 x) = (T c1 x)

--6
delete val tree = greyRebalance $ greyRebalance (delete' val tree) 	


delete' val (T c1 (N n t1 t2)) 	| val == n = (T c1 (N (leftmostValue t2) t1 (removeLeftmostNode t2)))
								| val < n = (T c1 (N n (delete' val t1) t2))
								| otherwise = (T c1 (N n t1 (delete' val t2)))

delete' val (T c1 L) = error "number not found"
























