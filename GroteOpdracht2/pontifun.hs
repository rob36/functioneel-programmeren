import Graphs
import Prelude
import Data.Maybe
import System.Random
import System.IO.Unsafe
import Data.List (sortBy, find, partition, (!!))
import EventLoop.Output

-- The bird data type
type Bird = (Pos,Vector,Int)

-- The store data type
data Store = Store {
    graph :: Graph
    , node1Select :: Maybe Node
    , delete :: Bool
    , level :: Int
    , time :: Int
    , birds :: [Bird]
}

-- The button data type. (name, left upper corner, right bottom corner, function to execute)
type Button = ([Char], Pos, Pos, (Store -> ([GraphOutput], Store)))
buttons = [("Delete", (10,10), (90,30),buttonDelete), ("Reset", (110,10), (190,30), buttonReset), ("Simulate", (210,10), (290,30), buttonSimulate), ("Previous Level", (canvasWidth-270,10), (canvasWidth-150,30), buttonPrevLevel), ("Next Level", (canvasWidth-130,10), (canvasWidth-10,30), buttonNextLevel) ] :: [Button]

-- The level graphs and backgrounds
levels :: [(Graph,GraphOutput)]
levels = [
        (Graph [((300, 400), Black, Anchor), ((700,500), Black, Anchor), ((1000, 400), Black, Anchor) ] [] Weighted, PlainG [
            Draw (GObject [] (Rect (139,69,19) 2.0 (222,184,135) (0, 390)   (310, canvasHeight-390)) []) [],
            Draw (GObject [] (Rect (139,69,19) 2.0 (222,184,135) (990, 390) (canvasWidth-990, canvasHeight-390)) []) [],
            Draw (GObject [] (Rect (139,69,19) 2.0 (222,184,135) (690, 490) (20, canvasHeight-490)) []) []
        ]),
        (Graph [((300, 400), Black, Anchor), ((1000, 400), Black, Anchor) ] [] Weighted, PlainG [
            Draw (GObject [] (Rect (139,69,19) 2.0 (222,184,135) (0, 390)   (310, canvasHeight-390)) []) [],
            Draw (GObject [] (Rect (139,69,19) 2.0 (222,184,135) (990, 390) (canvasWidth-990, canvasHeight-390)) []) []
        ])
    ]

-- The standard store at the beginning of the game.
beginStore :: Store
beginStore = Store {
        graph       = g,
        node1Select = Nothing,
        delete      = False,
        level       = 0,
        time        = 0,
        birds       = []
    }
    where
        (g,_)       = levels !! 0

-- Resets the store
resetStore s = s {
        node1Select = Nothing,
        delete      = False
    }

-- this function handles the events of the application.
eventhandler :: Store -> GraphInput -> ([GraphOutput], Store)
eventhandler s@(Store {level=lev}) Start = (drawGrid:bg:go++(map (drawButton False) buttons),s{graph=g})
    where
        go      = graphToOutput g
        (g,bg)  = levels !! lev

eventhandler s@(Store {time=t,birds=bs}) TimerTick = ([
                PlainG ([Draw (GObject name (Rect (0,0,255) 1.0 (0,0,255) (0,height) (canvasWidth, canvasHeight)) []) name,
                RemoveGroup nameold
            ]++bo)
        ],s{time=t',birds=bs''})
    where
        (bs'',bo)   = drawBirds bs' t'
        height      = realToFrac canvasHeight - 100.0 - sin ((realToFrac t')/50.0*pi) * 10.0
        nameold     = "water" ++ (show t)
        name        = "water" ++ (show t')
        t'          = mod (t+1) 100
        bs'         = randomBirdAdd bs

eventhandler s@(Store g ns1 del _ _ _) (MouseUp MLeft pos)
    | isJust f && isJust ns1                                                                = (go3++go9,s9)
    | isJust f                                                                              = (go8,s8)
    | del && isJust node && isNothing ns1                                                   = (go1, s' {graph = g1, node1Select = node, delete=True})   -- select node for deleteion of edgeToOutput
    | del && isJust node && (getPosition $ fromJust node) == (getPosition $ fromJust ns1)   = (go3++goDeleteUnsel, s' {graph = g3})                     -- Delete selected edge with possible free nodes
    | del && isJust node                                                                    = (go3++go7++goDeleteUnsel, s' {graph = g7})                -- Delete selected edge with possible free nodes
    | del && isJust ns1                                                                     = (go3++goDeleteUnsel,s' {graph = g3})                      -- Stop deleting
    | del                                                                                   = (goDeleteUnsel,s')                                        -- Stop deleting
    | isJust node && isNothing ns1                                                          = (go1, s' {graph = g1, node1Select = node})                -- Selecteer nieuwe node
    | isJust node && (getPosition $ fromJust node) == (getPosition $ fromJust ns1)          = (go3, s' {graph = g3})                                    -- deselect
    | isJust node                                                                           = (go2++go4, s' {graph = g4})                               -- teken lijntje tussen beide nodes
    | isJust ns1                                                                            = (go3++go5++go6, s' {graph = g6})                          -- teken lijntje tussen node en nieuwe node
    | otherwise                                                                             = ([],s')
    where
        s'                  = resetStore s
        node                = onNode (getNodes g) pos
        f                   = onButton (buttons) pos
        node'               = fromJust node
        ns1'                = fromJust ns1
        (go1,g1)            = colorNode Red node' g
        (go2,g2)            = colorNode Black ns1' g1
        (go3,g3)            = colorNode Black ns1' g
        (go4,g4)            = makeLine g2 ns1' node'
        (go5,g5,ns2)        = makeNode g3 pos
        (go6,g6)            = makeLine g5 ns1' ns2
        (go7,g7)            = removeEdgeG g3 ns1' node'
        (go8,s8)            = (fromJust f) s
        (go9,s9)            = (fromJust f) (s {graph = g3})
        delb@(dn,_,_,_)     = buttons !! 0
        goDeleteUnsel       = [PlainG [RemoveGroup dn],drawButton False delb]

eventhandler s _ = ([],s)

-- spawn a random bird randomly and add it to the list of birds.
randomBirdAdd :: [Bird] -> [Bird]
randomBirdAdd bs
    | length bs < 5 && r == 1   = ((px,py),(vx,vy),size):bs
    | otherwise = bs
    where
        r       = (unsafePerformIO $ getStdRandom $ randomR (1,200)) :: Int
        dir     = (unsafePerformIO $ getStdRandom $ randomR (0,1)) :: Int
        vx      = (-1+2*(realToFrac dir))*(unsafePerformIO $ getStdRandom $ randomR (3,10)) :: Float
        vy      = (unsafePerformIO $ getStdRandom $ randomR (-1,1)) :: Float
        size    = (unsafePerformIO $ getStdRandom $ randomR (10,20)) :: Int
        px      = realToFrac $ abs (canvasWidth * (dir -1))
        py      = (unsafePerformIO $ getStdRandom $ randomR (30,150)) :: Float

-- move all the birds and return the new bird list and the required graphical output.
drawBirds :: [Bird] -> Int -> ([Bird],[Graphical])
drawBirds [] _ = ([],[])
drawBirds ((p@(x,y),v,size):bs) time
    | realToFrac (-1 * size) < x && x < canvasWidth + realToFrac size && -1 * realToFrac size < y = ((p',v,size):bs',[
        Draw (GObject [] (Text (0,0,0) 0.0 col p' size' textFont text True) []) name,
        RemoveGroup nameold
    ]++go)
    | otherwise = (bs',[RemoveGroup nameold]++go)
    where
        nameold = "bird"++(show p)
        name    = "bird"++(show p')
        p'      = addVector p v
        (bs',go)= drawBirds bs time
        size'   = realToFrac size
        col     = (450-size'*20,450-size'*20,450-size'*20)
        text    = if ((mod time 10) < 5) then "v" else "V"

-- check if a button is pressed.
onButton :: [Button] -> Pos -> Maybe (Store -> ([GraphOutput], Store))
onButton ((_,(xlb,ylb),(xro,yro),f):bs) (x,y)
    | xlb < x && x < xro && ylb < y && y < yro = Just f
    | otherwise = onButton bs (x,y)

onButton [] pos = Nothing

-- the function executed if the delete button is pressed
buttonDelete :: Store -> ([GraphOutput], Store)
buttonDelete s = ([PlainG [RemoveGroup dn],drawButton True delb],s'{delete = True})
    where
        s'                  = resetStore s
        delb@(dn,_,_,_)     = buttons !! 0

-- The function executed if the reset button is pressed
buttonReset :: Store -> ([GraphOutput], Store)
buttonReset s@(Store {level = lev}) = (ClearAllG:go,s2)
    where
        s'              = resetStore s
        (go,s2)         = eventhandler s' Start

-- the function executed if the simulate button is pressed
buttonSimulate :: Store -> ([GraphOutput], Store)
buttonSimulate s@(Store {graph=g@(Graph {nodes=ns,edges=es})}) = (gorem++goadd,s'{graph=g'})
    where
        s'      = resetStore s
        es'     = updateWeight es (calcGravity (buildVNodes ns) es)
        g'      = g{edges=es'}
        gorem   = map (\(p1,p2,_,_) -> RemoveEdgeG p1 p2) es
        goadd   = map edgeToOutput es'

-- the function executed if the next level button is pressed
buttonNextLevel :: Store -> ([GraphOutput], Store)
buttonNextLevel s@(Store {level = lev}) = (ClearAllG:go,s2)
    where
        s'      = resetStore s
        lev'    = mod (lev+1) (length levels)
        (go,s2) = eventhandler (s'{level = lev'}) Start

-- the function executed if the previous level button is pressed
buttonPrevLevel :: Store -> ([GraphOutput], Store)
buttonPrevLevel s@(Store {level = lev}) = (ClearAllG:go,s2)
    where
        s'      = resetStore s
        lev'    = mod (lev-1+(length levels)) (length levels)
        (go,s2) = eventhandler (s'{level = lev'}) Start

-- colors a node the given color
colorNode :: ColorG -> Node -> Graph -> ([GraphOutput], Graph)
colorNode col (p, c, a) g = (outN, g'')
    where
        g'           = removeNode g p
        n'           = (p, col, a)
        g''          = addNode g' n'
        outN         = [RemoveNodeG p, nodeToOutput n']

-- adds a node to the graph and gives the required graph output.
makeNode :: Graph -> Pos -> ([GraphOutput], Graph, Node)
makeNode g p = (outN, g', n)
    where
        n       = (p, Black, Free)
        g'      = addNode g n
        outN    = [nodeToOutput n]

-- adds a line to the graph and gives the required graph output.
makeLine :: Graph -> Node -> Node -> ([GraphOutput], Graph)
makeLine g ns1@(p1,_,_) ns2@(p2,_,_)
    | hasEdge g ns1 ns2   = ([], g)
    | otherwise           = ([edgeToOutput e], g')
    where
        e = (p1,p2,Black,0)
        g' = addEdge g e

-- removes a line from the graph and gives the required graph output.
removeEdgeG :: Graph -> Node -> Node -> ([GraphOutput], Graph)
removeEdgeG g n1@(pos1,_,_) n2@(pos2,_,_)
    | isNothing e               = ([], g)
    | not n1free && not n2free  = ([RemoveEdgeG ep1 ep2], g1)
    | n1free && n2free          = ([RemoveEdgeG ep1 ep2, RemoveNodeG pos1, RemoveNodeG pos2], g4)
    | n1free                    = ([RemoveEdgeG ep1 ep2, RemoveNodeG pos1], g2)
    | n2free                    = ([RemoveEdgeG ep1 ep2, RemoveNodeG pos2], g3)
    where
        e       = findEdge g pos1 pos2
        (ep1,ep2,_,_) = fromJust e
        g1      = removeEdge g pos1 pos2
        g2      = removeNode g1 pos1    -- remove p1
        g3      = removeNode g1 pos2    -- remove p2
        g4      = removeNode g2 pos2    -- remove p1&p2
        n1free  = not $ hasEdgeSingleNode g1 n1
        n2free  = not $ hasEdgeSingleNode g1 n2

-- starts the application
start :: IO ()
start = preEventloop eventhandler beginStore
--------------------------------------------------------------- vector stuff ----------------------------------------------------------------
-- the vector node data type
type VNode = (Pos,Vector,Anchor)

-- sorts the nodes by height.
sortNodes :: [Node] -> [Node]
sortNodes nodes = sortBy (\((_,y1),_,_) ((_,y2),_,_) -> compare y1 y2) nodes

-- convert all nodes to vnodes
buildVNodes :: [Node] -> [VNode]
buildVNodes ns = map (\(p,_,a) -> (p, (0,0),a)) (sortNodes ns)

-- calculate the force on all nodes.
calcGravity :: [VNode] -> [Edge] -> [VNode]
calcGravity [] e = []
calcGravity (n@(p,_,Anchor):ns) e = (p,(0,0),Anchor):(calcGravity ns e')
    where
        (reach, e') = partition (\(p1,p2,_,_) -> p == p1 || p ==p2) e
calcGravity (n@(p,v,Free):ns) e = n:(calcGravity ns'' e')
    where
        (reach, e') = partition (\(p1,p2,_,_) -> p == p1 || p ==p2) e
        ns' = map (calcGravity' reach p) ns
        alphaV = vectorToAngle v
        (weighted,rest) = splitAt 2 (
            sortBy (\(a1,_) (a2,_) -> compare a1 a2) (
                map (\x -> (abs (calcAngle p (getOtherPos x p)-alphaV),x)) reach
            ))
        ns'' = map (calcGravity'' (vectorSize v) weighted) ns'


calcGravity' :: [Edge] -> Pos -> VNode -> VNode
calcGravity' es ptop (p,v,a)
    | isJust e  = (p,addVector v (edgeToVector e' ptop),a)
    | otherwise = (p,v,a)
    where
        e  = find (\(p1,p2,_,_) -> p == p1 || p == p2) es
        e' = fromJust e

calcGravity'' :: Float -> [(Float,Edge)] -> VNode -> VNode
calcGravity'' vsize ws (p,v,an)
    | isJust w  = (p,v',an)
    | otherwise = (p,v,an)
    where
        w     = find (\(_,(p1,p2,_,_)) -> p == p1 || p == p2) ws
        (a,e) = fromJust w
        h     = vsize * cos a
        alpha = calcAngle (getOtherPos e p) p
        x     = sin alpha * h
        y     = cos alpha * h
        v'    = addVector (x,y) v

-- return the other pos in the node
getOtherPos :: Edge -> Pos -> Pos
getOtherPos (p1,p2,_,_) p
    | p == p1   = p2
    | otherwise = p1

-- convert a vector to an angle
vectorToAngle :: Vector -> Float
vectorToAngle (x,y)
    | y == 0 && x /= 0  = pi/2
    | y == 0            = 0
    | otherwise         = atan (x/y)

-- calculate the angle between two points.
calcAngle :: Pos -> Pos -> Float
calcAngle (x1,y1) (x2,y2)
    | y2-y1 == 0 && (x2-x1) /= 0 = pi/2
    | y2-y1 == 0                 = 0
    | otherwise                  = atan ((x2-x1)/(y2-y1))

-- calculates the force on the edge.
forceOnEdge :: [VNode] -> Edge -> Weight
forceOnEdge ns (p1,p2,_,_) = round $ vectorSize $ addVector v (-x,-y)
    where
        (_,v,_)     = fromJust $ find (\(p,_,_) -> p == p1) ns
        ((x,y),_,_) = fromJust $ find (\(p,_,_) -> p == p2) ns

-- updates the weight on the nodes.
updateWeight :: [Edge] -> [VNode] -> [Edge]
updateWeight es vns = map (\e@(p1,p2,c,_) -> (p1,p2,c,forceOnEdge vns e)) es

------ Vector helper functions -------

-- Returns the sum of the vectors
addVector :: Vector -> Vector -> Vector
addVector (v1x,v1y) (v2x, v2y) = (v1x+v2x, v1y+v2y)

-- The multiplication of a vector with a float
facMulVector :: Float -> Vector -> Vector
facMulVector n (vx,vy) = (n*vx,n*vy)

-- The dot product of two vectors
dotProduct :: Vector -> Vector -> Float
dotProduct (v1x,v1y) (v2x,v2y) = (v1x*v2x) + (v1y*v2y)

-- convert an edge to a vector where the given position is the starting one of the edge.
edgeToVector :: Edge -> Pos -> Vector
edgeToVector (p1,p2,_,_) p
    | p == p1   = vectorize p p2
    | otherwise = vectorize p p1
