module Graphs
        ( Weight
        , Node
        , Edge
        , Graph(..)
        , ColorG(..)
        , Thickness(..)
        , Anchor(..)
        , Weighted(..)
        , GraphOutput(..)
        , GraphInput(..)
        , Pos
        , Vector
        , vectorSize
        , MouseButton(..)
        , KeyboardButton
        , onNode
        , preEventloop
        , getNodes
        , vectorize
        , canvasWidth
        , canvasHeight
        , textFont

        , removeNode
        , removeEdge
        , addNode
        , addEdge

        , nodeToOutput
        , edgeToOutput
        , graphToOutput

        , getPosition

        , findEdge
        , findNode

        , hasEdge
        , hasEdgeSingleNode

        , drawGrid
        , drawButton
        ) where

import Prelude
import EventLoop
import EventLoop.Input as EI
import EventLoop.Output
import Data.Maybe
import Data.List (delete,deleteBy)

type Vector = (Float, Float)

----- Graph -----

type Weight  = Int

type Node = (Pos, ColorG, Anchor)

-- returns the postition of a node
getPosition :: Node -> Pos
getPosition (p,_,_) = p

-- rounds positions to ten
roundToTen :: Pos -> Pos
roundToTen (x,y) = (
    gridinterval*(fromIntegral (round (x/gridinterval))),
    gridinterval*(fromIntegral (round (y/gridinterval))))

type Edge = (Pos, Pos, ColorG, Weight)

data Graph = Graph
            { nodes    :: [Node]
            , edges    :: [Edge]
            , weighted :: Weighted
            } deriving (Eq, Show)

-- returns the list of nodes in a graph
getNodes :: Graph -> [Node]
getNodes Graph{nodes=r} = r

----- Graph Graphical -----

data ColorG = Red
            | Blue
            | Green
            | Purple
            | Grey
            | Yellow
            | Orange
            | Black
            | White
            | RGB Color
            deriving (Eq, Show)

data Anchor = Anchor
            | Free
            deriving (Eq, Show)

data Thickness = Thin
               | Thick
               deriving (Show)

data Weighted  = Weighted
               | Unweighted
               deriving (Eq, Show)

-- | The output expected in a graph graphical program
data GraphOutput = NodeG Pos ColorG
                 | AnchorG Pos ColorG
                 | LineG Pos Pos ColorG Thickness
                 | WeightedLineG Pos Pos Weight ColorG Thickness
                 | Instructions [String]
                 | RemoveNodeG Pos
                 | RemoveEdgeG Pos Pos
                 | PlainG [Graphical]
                 | ClearAllG

nodeRadius      = 10    :: Float
textSize        = 16    :: Float
textFont        = "Courier"
xArrowSize      = 6     :: Float
yArrowSize      = 6     :: Float
weightHeight    = 10    :: Float

topInstructions = 440
canvasWidth     = 1500
canvasHeight    = 800
dimCanvas       = (canvasWidth,canvasHeight)
gridthick       = 1
gridcol         = (200,200,200)
gridinterval    = 20.0

-- | The input expected in a graph graphical program
data GraphInput = MouseUp MouseButton Pos
                | MouseDown MouseButton Pos
                | MouseClick MouseButton Pos
                | KeyPress KeyboardButton
                | TimerTick
                | Start

-- | Checkes to see if there is a node on a certain position
onNode :: [Node] -> Pos -> Maybe Node
onNode [] _ = Nothing
onNode (n@((nx, ny), _, Free):ns) (x,y)
    | difference <= nodeRadius = Just n
    | otherwise             = onNode ns (x,y)
    where
        dx         = nx - x
        dy         = ny - y
        difference = sqrt (dx^2 + dy^2)

-- there is another click box around anchor nodes.
onNode (n@((nx, ny), _, Anchor):ns) (x,y)
    | nx - nodeRadius < x && x < nx + nodeRadius && ny - nodeRadius < y && y < ny + nodeRadius = Just n
    | otherwise = onNode ns (x,y)

-- | Starting point for the library. Call this function to start the eventloop with the given handler.
-- Takes 'FPPrac.Graph.Input' and returns 'FPPrac.Graph.Output' instead of the standardized 'EventLoop.Input'
-- and 'EventLoop.Output'.
preEventloop :: (a -> GraphInput -> ([GraphOutput], a)) -> a -> IO ()
preEventloop handler beginState = start handler' beginState
                                where
                                    handler' = changeTypes handler

-- | Changes the eventhandler to use the 'Graphs.GraphInput' instead of the 'EventLoop.Input.InputEvent'
-- Also catches the 'EventLoop.Input.InSysMessage.Setup' message and gives the correct dimensions.
changeTypes :: (a -> GraphInput -> ([GraphOutput], a)) -> a -> EI.InputEvent -> ([OutputEvent], a)
changeTypes _       state (EI.InSysMessage Setup) = ([OutSysMessage [CanvasSetup dimCanvas,Timer (On 17)]],state)
changeTypes handler state inputE = (out, state')
    where
        inputE'            = inputEventToGraphIn inputE
        (graphOut, state') = handler state inputE'
        out                = map (\a -> OutGraphical a) $ concat $ map graphOutputToGraphical graphOut

-- | Abstracts the standardized 'EventLoop.Input.InputEvent' to 'Graphs.GraphInput'
inputEventToGraphIn :: EI.InputEvent -> GraphInput
inputEventToGraphIn (EI.InKeyboard k) = keyboardToGraphIn k
inputEventToGraphIn (EI.InMouse m )   = mouseToGraphIn m
inputEventToGraphIn (EI.InSysMessage Background) = Start
inputEventToGraphIn (EI.InSysMessage Time) = TimerTick

-- | Abstracts the standardized 'EventLoop.Input.Mouse' to 'Graphs.GraphInput'
mouseToGraphIn :: EI.Mouse -> GraphInput
mouseToGraphIn (EI.MouseClick mb p _) = Graphs.MouseClick mb (roundToTen p)
mouseToGraphIn (EI.MouseUp    mb p _) = Graphs.MouseUp mb (roundToTen p)
mouseToGraphIn (EI.MouseDown  mb p _) = Graphs.MouseDown mb (roundToTen p)

-- | Abstracts the standardized 'EventLoop.Input.Keyboard' to 'Graphs.GraphInput'
keyboardToGraphIn :: EI.Keyboard -> GraphInput
keyboardToGraphIn (EI.KeyPress k) = Graphs.KeyPress k

{-
Converts the graphical graph output to standardized 'Eventloop.Output.Graphical' output
-}
graphOutputToGraphical :: GraphOutput -> [Graphical]
graphOutputToGraphical (NodeG pos colG) = [Draw (Container [nodeG]) l]
    where
        l       = show pos
        col     = colorGToColor colG
        nodeG   = GObject l (Arc black 1 col pos nodeRadius 0 360) []
        -- textG    = GObject l (Text red 1 red pos textSize textFont l True) []

graphOutputToGraphical (AnchorG (x,y) colG) = [Draw (Container [anchorG]) l]
    where
        l       = show (x,y)
        col     = colorGToColor colG
        anchorG = GObject l (Rect black 1 col (x-nodeRadius,y-nodeRadius) (nodeRadius*2,nodeRadius*2)) []
        -- textG    = GObject l (Text red 1 red (x,y) textSize textFont l True) []

graphOutputToGraphical (LineG pos1 pos2 colG thick) = [Draw (Container [line]) name]
    where
        name    = lineName pos1 pos2
        col     = colorGToColor colG
        thick'  = thicknessToFloat thick
        line    = GObject name (Line col thick' [lineStart, lineEnd]) []
        arrow1  = GObject name (Line col thick' [arrowStart, arrow1End]) []
        arrow2  = GObject name (Line col thick' [arrowStart, arrow2End]) []
        --Vector stuff
        lineVector          = vectorize pos1 pos2
        lineVector'         = vectorize pos2 pos1
        lineStart           = posOnVector nodeRadius lineVector pos1
        lineEnd             = posOnVector nodeRadius lineVector' pos2
        arrowPerpStart      = posOnVector xArrowSize lineVector' lineEnd
        upPerpLineVector    = upPerpendicularTo pos1 pos2
        downPerpLineVector  = downPerpendicularTo pos1 pos2
        arrowStart          = lineEnd
        arrow1End           = posOnVector yArrowSize upPerpLineVector arrowPerpStart
        arrow2End           = posOnVector yArrowSize downPerpLineVector arrowPerpStart

graphOutputToGraphical (WeightedLineG pos1 pos2 w colG thick) = lineGraphical-- ++ [Draw text name]
    where
        name                = lineName pos1 pos2
        col                 = colorGToColor colG'
        colG'               = if (w<900) then (RGB (0,255,0)) else (if (w<1200) then (RGB (255,127,0)) else (RGB (255,0,0)))
        lineGraphical       = graphOutputToGraphical (LineG pos1 pos2 colG' thick)
        text                = GObject name (Text col 1 col textPos textSize textFont (show w) True) []
        --Vector stuff
        lineVector'         = vectorize pos2 pos1
        halfSize            = vectorSize lineVector' / 2
        upPerpLineVector    = upPerpendicularTo pos2 pos1
        textPerpStart       = posOnVector halfSize lineVector' pos2
        textPos             = posOnVector weightHeight upPerpLineVector textPerpStart

graphOutputToGraphical (Instructions is) = [RemoveGroup "instructions", Draw isG' "instructions"]
    where
        lineG       = GObject "instructions" (Line black lineHeight [(0,topInstructions), (canvasWidth, topInstructions)]) []
        defaultText = (\str pos -> GObject "instructions" (Text black 1 black pos textSize textFont str False) [])
        lineHeight  = 2
        textMargin  = 2
        positions   = iterate ((+) (textSize + textMargin)) (topInstructions + lineHeight)
        isWithPos   = zip is positions
        isG         = map (\(str, top) -> defaultText str (0, top)) isWithPos
        isG'        = Container (lineG:isG)

graphOutputToGraphical (PlainG gs) = gs

graphOutputToGraphical (RemoveNodeG p)     = [RemoveGroup (show p)]
graphOutputToGraphical (RemoveEdgeG p1 p2) = [RemoveGroup (lineName p1 p2)]
graphOutputToGraphical (ClearAllG)         = [ClearAll]

-- | Returns a standardized naming scheme for lines in the graph
lineName :: Pos -> Pos -> String
lineName p1 p2 = "line."++(show p1)++"."++(show p2)

-- | Translates the thickness to a float
thicknessToFloat :: Thickness -> Float
thicknessToFloat Thick = 2.0
thicknessToFloat Thin  = 1.0

-- | Translates color datatype to RGB codes
colorGToColor :: ColorG -> Color
colorGToColor Red       = (255, 0, 0)
colorGToColor Blue      = (0, 0, 255)
colorGToColor Green     = (0, 255, 0)
colorGToColor Purple    = (255, 0, 255)
colorGToColor Grey      = (125, 125, 125)
colorGToColor Yellow    = (255, 255, 0)
colorGToColor Orange    = (255, 125, 0)
colorGToColor Black     = (0, 0, 0)
colorGToColor White     = (255, 255, 255)
colorGToColor (RGB c)   = c

black   = colorGToColor Black
white   = colorGToColor White
red     = colorGToColor Red


-- | Returns the point when making a step f long from the point start in the direction of the vector. The length between start pos and result pos is always f.
posOnVector :: Float -> Vector -> Pos -> Pos
posOnVector f (xv, yv) (xStart, yStart) = (x, y)
                                        where
                                            x        = xStart + fraction * xv
                                            y        = yStart + fraction * yv
                                            fraction = f / size
                                            size     = vectorSize (xv, yv)

-- | Vector from p1 to p2
vectorize :: Pos -> Pos -> Vector
vectorize (x1, y1) (x2, y2) = (x2 - x1, y2 - y1)


-- | Returns the vector perpendicular on the given vector between the 2 points. Always has positive y and vector length 1; y is inverted in canvas
downPerpendicularTo :: Pos -> Pos -> Vector
downPerpendicularTo (x1, y1) (x2, y2)
    | y2 > y1   = ((-1) * sign * (abs yv) / size, (abs xv) / size)
    | otherwise = (       sign * (abs yv) / size, (abs xv) / size)
    where
        (xv, yv) = vectorize (x1, y1) (x2, y2)
        size       = vectorSize (xv, yv)
        sign       = xv / (abs xv)


-- | Returns the vector perpendicular on the given vector between the 2 points. Always has negative y and vector length 1; y is inverted in canvas
upPerpendicularTo :: Pos -> Pos -> Vector
upPerpendicularTo p1 p2 = ((-1) * xp, (-1) * yp)
    where
        (xp, yp) = downPerpendicularTo p1 p2

-- | Returns the size of the vector
vectorSize :: Vector -> Float
vectorSize (x, y) = sqrt (x^2 + y^2)


-- Returns a list of graph outputs to draw a graph
graphToOutput :: Graph -> [GraphOutput]
graphToOutput g@(Graph {edges=es, nodes=ns}) = (map nodeToOutput ns) ++ (map edgeToOutput es)

-- removes a node from the graph
removeNode :: Graph -> Pos -> Graph
removeNode g@(Graph{nodes=ns}) p    | nM == Nothing = g
                                    | otherwise     = g {nodes = ns'}
                                    where
                                        nM = findNode g p
                                        n  = fromJust nM
                                        ns' = delete n ns

-- removes an edge from the graph
removeEdge :: Graph -> Pos -> Pos -> Graph
removeEdge g@(Graph{edges=es}) p1 p2 = g {edges=es'}
    where
        es' = filter (\(ep1,ep2,_,_) -> not ((p1 == ep1 && p2 == ep2) || (p1 == ep2 && p2 == ep1))) es

-- adds a node to the graph
addNode :: Graph -> Node -> Graph
addNode g@(Graph{nodes=ns}) n = g {nodes=(n:ns)}

-- adds an edge to the graph
addEdge :: Graph -> Edge -> Graph
addEdge g@(Graph{edges=es}) e = g {edges=(e:es)}

-- finds the node on the given postion if there is one there
findNode :: Graph -> Pos -> Maybe Node
findNode (Graph {nodes=nodes}) p
    | null n = Nothing
    | otherwise = Just (head n)
    where
        n = filter (\(np, _, _) -> p == np) nodes

-- converts a node/anchor to a graphical node/anchor
nodeToOutput :: Node -> GraphOutput
nodeToOutput (p,c,Free)     = NodeG p c
nodeToOutput (p,c,Anchor)   = AnchorG p c

-- converts an edge to a graphical edge
edgeToOutput :: Edge -> GraphOutput
edgeToOutput (p1, p2, c, w) = WeightedLineG p1 p2 w c Thin

-- returns the nodes at the end the edge
findNodesByEdge :: Graph -> Edge -> (Node, Node)
findNodesByEdge g (p1, p2, _, _) = (n1, n2)
    where
        Just n1 = findNode g p1
        Just n2 = findNode g p2

-- returns the edge between the given positions if there is such an edge
findEdge :: Graph -> Pos -> Pos -> Maybe Edge
findEdge (Graph {edges=es}) p1 p2
    | null e    = Nothing
    | otherwise = Just (head e)
    where
        e = filter (\(ep1, ep2, _, _) -> (p1 == ep1 && p2 == ep2) || (p1 == ep2 && p2 == ep1)) es

-- returns wether or not the given nodes have an edge between them
hasEdge :: Graph -> Node -> Node -> Bool
hasEdge g n1 n2 = isJust $ findEdge g (getPosition n1) (getPosition n2)

-- returns wether or not a node has a edge (an anchor is connected to the ground so it always has an edge
hasEdgeSingleNode :: Graph -> Node -> Bool
hasEdgeSingleNode (Graph {edges=es}) (p,_,Free) = any (\(p1,p2,_,_) -> p1 == p || p2 == p) es
hasEdgeSingleNode _ _ = True

-- gives the graph output required to draw a grid
drawGrid :: GraphOutput
drawGrid = PlainG (
    (
        map (
            \x -> Draw (
                GObject name (Line gridcol gridthick [(x,0),(x,canvasHeight)]) []
            ) name
        ) [0.0,gridinterval..canvasWidth]
    ) ++ (
        map (
            \y -> Draw (
                GObject name (Line gridcol gridthick [(0,y),(canvasWidth,y)]) []
            ) name
        ) [0.0,gridinterval..canvasHeight]
    ))
    where
        name = "grid"

-- returns the color of a button in the given state
getSelectColor :: Bool -> Color
getSelectColor True = red
getSelectColor False = black

-- returns the graphical output required to draw the given button
drawButton :: Bool -> ([Char],Pos,Pos,a) -> GraphOutput
drawButton sel (name, (xlb,ylb), (xro,yro),_) = PlainG (
        [ Draw (Container [rect,text]) name ]
    )
    where
        col  = getSelectColor sel
        ncol = getSelectColor (not sel)
        rect = GObject [] (Rect col 1 col (xlb,ylb) (xro-xlb,yro-ylb)) []
        text = GObject [] (Text ncol 0 ncol (xlb+(xro-xlb)/2,ylb+(yro-ylb)/2) textSize textFont name True) []
