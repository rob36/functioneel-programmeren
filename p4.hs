import FPPrac.Trees.ParseTree
import FPPrac
import Data.Either

exampleParseTree = showTree exampleTree

-- opgave 1
-- a
data BinTree a b = BinLeaf b | BinNode a (BinTree a b) (BinTree a b)

-- b
---- 1a
type Tree1a = BinTree Number Number
---- 1b
type Tree1b = BinTree (Number,Number) (Number,Number)
---- 1c
data Unit = Unit

instance Show Unit where
	show Unit = ""
type Tree1c = BinTree Number Unit

-- c
pp :: (Show a, Show b) => BinTree a b -> ParseTree
pp (BinLeaf b) = ParseNode (show b) []
pp (BinNode a t1 t2) = ParseNode (show a) [pp t1,pp t2]

parseTreeToString (ParseNode a x) = " n:"++(show a)++"{ "++(concat (map parseTreeToString x))++" } "

-- opgave 2
charToOp :: (Integral a, Fractional a) => Char -> a -> a -> a
charToOp x	| x == '+' = (+)
		| x == '-' = (-)
		| x == '*' = (*)
		| x == '/' = (/)
		| x == '^' = (^)	
                | otherwise = error "Operation not supported"

-- 1
parse1
  :: (Fractional a, Integral a) =>
     [Char] -> BinTree (a -> a -> a) Number
parse1 x = result
	where
		(result, _) = parse1' x

--parse1' :: [Char] -> (BinTree Char Number, [Char])
parse1' (x:xs) 	| elem x ['0'..'9'] = (BinLeaf ((ord x)-(ord '0')),xs)
		| '(' == x =  (BinNode (charToOp (head rest1)) result1 result2, tail rest2)
                | otherwise = error "parse error" 
			where
				(result1, rest1) = parse1' xs
				(result2, rest2) = parse1' (tail rest1)

-- 2
parse2 :: [Char] -> BinTree Char (Either Number Char)
parse2 x = result
	where
		(result,_) = parse2' x

parse2' :: [Char] -> (BinTree Char (Either Number Char), [Char])
parse2' (x:xs) 	| elem x ['0'..'9'] = (BinLeaf (Left ((ord x)-(ord '0'))),xs)
		| elem x (['a'..'z']++['A' .. 'Z']) = (BinLeaf (Right x),xs)
		| '(' == x =  (BinNode ((head rest1)) result1 result2::BinTree Char (Either Number Char), tail rest2)
                | otherwise = error "parse error" 
			where
				(result1, rest1) = parse2' xs
				(result2, rest2) = parse2' (tail rest1)

-- 3
parse3 :: [Char] -> BinTree Char (Either Number [Char])
parse3 x = result
	where
		(result,_) = parse3' x

parse3' :: [Char] -> (BinTree Char (Either Number [Char]), [Char])
parse3' (x:xs) 	| elem x ['0'..'9'] = (BinLeaf (Left value), rest3)
		| elem x (['a'..'z']++['A' .. 'Z']) = (BinLeaf (Right name), rest4)
		| '(' == x =  (BinNode ((head rest1)) result1 result2, tail rest2)
                | otherwise = error "parse error"
			where
				(result1, rest1) = parse3' xs
				(result2, rest2) = parse3' (tail rest1)
				(value,   rest3) = parseNumber 0 (x:xs) 
				(name,    rest4) = parseVariable [] (x:xs) 

parseNumber :: Number -> [Char] -> (Number, [Char])
parseNumber val (x:xs)  | elem x ['0' .. '9'] = parseNumber ((val*10)+(ord x)-(ord '0')) xs
                        | otherwise = (val, (x:xs))

parseVariable :: [Char] -> [Char] -> ([Char], [Char])
parseVariable val (x:xs) 	| elem x (['a'..'z']++['A' .. 'Z']) = parseVariable (val++[x]) xs	
				| otherwise = (val, (x:xs))

-- 4
parse4 :: [Char] -> BinTree (Number -> Number -> Number) (Either Number [Char])
parse4 x = result
	where
		(result,_) = parse4' x

parse4' :: [Char] -> (BinTree (Number -> Number -> Number) (Either Number [Char]), [Char])
parse4' (x:xs) 	| x == '-' = (BinLeaf (Left (-1*nvalue)), rest5)
		| x == '.' = (BinLeaf (Left zvalue), rest3)
                | elem x (['0'..'9']++['.']) = (BinLeaf (Left value), rest3)
		| elem x (['a'..'z']++['A' .. 'Z']) = (BinLeaf (Right name), rest4)
		| '(' == x = (BinNode (charToOp (head rest1)) result1 result2, tail rest2)
                | otherwise = error "parse error"
			where
				(result1, rest1) = parse4' xs
				(result2, rest2) = parse4' (tail rest1)
				(value,   rest3) = parseNumber4 [] (x:xs) 
				(nvalue,  rest5) = parseNumber4 [] xs
				(zvalue,  rest6) = parseNumber4 [] ('0':x:xs)
				(name,    rest4) = parseVariable [] (x:xs)
				

parseNumber4 :: [Char] -> [Char] -> (Number, [Char])
parseNumber4 val (x:xs)  | elem x (['0' .. '9']++['.']) = parseNumber4 (val++[x]) xs
                         | otherwise = ((read val :: Number), (x:xs))

--5

testAssign name = 42
{-	tmp <- getLine
	read tmp :: Number
-}

eval :: ([Char] -> Number) -> BinTree (Number -> Number -> Number) (Either Number [Char]) -> Number
eval assign (BinLeaf (Left num)) = num
eval assign (BinLeaf (Right name)) = assign name
eval assign (BinNode f t1 t2) = f (eval assign t1) (eval assign t2)












