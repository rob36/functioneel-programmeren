import FPPrac
import RoseTree
import Data.List.Split

testtree = RoseNode "test" []


--opgave1
--a
data Tree1a = Leaf1a Number | Node1a Number Tree1a Tree1a
testtree1a = Node1a 8 (Node1a 8 (Leaf1a 6) (Leaf1a 4)) (Leaf1a 0)  

pp1a :: Tree1a -> RoseTree
pp1a (Leaf1a n) = RoseNode (show n) []                       
pp1a (Node1a n t1 t2) = RoseNode (show n) [pp1a t1, pp1a t2]

--b
data Tree1b = Leaf1b (Number,Number) | Node1b (Number,Number) Tree1b Tree1b
testtree1b = Node1b (8,8) (Node1b (8,6) (Leaf1b (6,3)) (Leaf1b (4,2))) (Leaf1b (0,5))  

pp1b (Leaf1b (n1,n2)) = RoseNode ("(" ++ (show n1) ++ "," ++ (show n2) ++ ")") []
pp1b (Node1b (n1,n2) t1 t2 ) =  RoseNode ("(" ++ (show n1) ++ "," ++ (show n2) ++ ")") [pp1b t1, pp1b t2]

--c
data Tree1c = Leaf1c | Node1c Number Tree1c Tree1c
testtree1c = Node1c 8 (Node1c 8 (Leaf1c) (Leaf1c)) (Leaf1c)  

pp1c :: Tree1c -> RoseTree
pp1c (Leaf1c) = RoseNode "" []                       
pp1c (Node1c n t1 t2) = RoseNode (show n) [pp1c t1, pp1c t2]

--d
data Tree1d = Leaf1d (Number,Number) | Node1d [Tree1d]
testtree1d = Node1d [Node1d [], Leaf1d (6,1)]  

pp1d :: Tree1d -> RoseTree
pp1d (Leaf1d (n1,n2)) = RoseNode ("(" ++ (show n1) ++ "," ++ (show n2) ++ ")") []                       
pp1d (Node1d trees) = RoseNode ("") (map pp1d trees)

--opgave2
--a
telop1a :: Tree1a -> Number -> Tree1a
telop1a (Leaf1a n) x = Leaf1a (n+x)
telop1a (Node1a n t1 t2) x = Node1a (n+x) (telop1a t1 x)  (telop1a t2 x)

--b
kwadr1a :: Tree1a-> Tree1a
kwadr1a (Leaf1a n) = Leaf1a (n^2)
kwadr1a (Node1a n t1 t2) = Node1a (n^2) (kwadr1a t1)  (kwadr1a t2)

--c
maptree :: (Number -> Number) -> Tree1a -> Tree1a
maptree f (Leaf1a n)  = Leaf1a (f n)
maptree f (Node1a n t1 t2) = Node1a (f n) (maptree f t1 )  (maptree f t2)
kwadr1amap = maptree (^2)
telop1amap x = maptree (+x)

--d
telopNode (Leaf1b (n1,n2)) = RoseNode (show (n1+n2)) []
telopNode (Node1b (n1,n2) t1 t2 ) =  RoseNode (show (n1+n2)) [telopNode t1, telopNode t2]

--e
maptree1b :: ((Number,Number) -> Number ) -> Tree1b -> Tree1a
maptree1b f (Leaf1b (n1,n2))  = Leaf1a (f(n1,n2))
maptree1b f (Node1b (n1,n2) t1 t2) = Node1a (f(n1,n2)) (maptree1b f t1) (maptree1b f t2)
maptree1bopt = maptree1b (\(n1,n2) -> n1+n2)
maptree1baft = maptree1b (\(n1,n2) -> n1-n2)
maptree1bverm = maptree1b (\(n1,n2) -> n1*n2)

--opgave3
--a
pp1arev :: Tree1a -> Tree1a
pp1arev (Leaf1a n) = Leaf1a n                       
pp1arev (Node1a n t1 t2) = Node1a n (pp1arev t2) (pp1arev t1)

--b
pp1drev :: Tree1d -> Tree1d
pp1drev (Leaf1d (n1,n2)) = Leaf1d (n2,n1)                       
pp1drev (Node1d trees) = Node1d (reverse (map pp1drev trees))

--opgave 4
--a
t_insert x (Leaf1c) = Node1c x (Leaf1c) (Leaf1c)
t_insert x (Node1c n t1 t2) | x<n = Node1c n (t_insert x t1) t2
                            | x==n = Node1c n (Node1c x t1 (Leaf1c)) t2
                            | otherwise = Node1c n t1 (t_insert x t2)

--b
makeTree :: [Number] -> Tree1c
makeTree [] = Leaf1c
makeTree [x] = Node1c x (Leaf1c) (Leaf1c)
makeTree (x:xs) = t_insert x (makeTree xs)

makeTree' :: [Number] -> Tree1c
makeTree' = foldr t_insert Leaf1c

--c
makeList :: Tree1c -> [Number]
makeList (Leaf1c) = []
makeList (Node1c n t1 t2) = (makeList t1) ++ [n] ++ (makeList t2)

--d
noobSort :: [Number] -> [Number]
noobSort = makeList . makeTree

--e
treeSort :: Tree1c -> Tree1c
treeSort = makeTree . makeList

--opgave 5
zoek :: Number -> Tree1c -> Tree1c
zoek n (Leaf1c) = error "Tree not available"
zoek n (Node1c x t1 t2) | n<x  = zoek n t1
                        | n==x = (Node1c x t1 t2)
                        | otherwise = zoek n t2

--opgave 6
totDiepte :: Number -> Tree1a -> Tree1a
totDiepte n (Leaf1a x) = Leaf1a x
totDiepte 0 (Node1a x t1 t2) = Leaf1a x
totDiepte n (Node1a x t1 t2) = Node1a x (totDiepte (n-1) t1) (totDiepte (n-1) t2)

--opgave 7
--a
vervang :: String -> Number -> Tree1a -> Tree1a
vervang [] n (Leaf1a g) = Leaf1a n
vervang [] n (Node1a g t1 t2) = Node1a n t1 t2
vervang (x:xs) n a = a
vervang (x:xs) n (Node1a g t1 t2) | x == 'l' = Node1a g (vervang xs n t1) t2
                                  | otherwise= Node1a g t1 (vervang xs n t2)

--b
subboom :: String -> Tree1a -> Tree1a
subboom [] tree = tree
subboom (x:xs) (Leaf1a g) = error "Te lang pad"
subboom (x:xs) (Node1a g t1 t2) | x == 'l' = subboom xs t1
                                | otherwise= subboom xs t2

--opgave 8
--a
dieptes :: Number -> Tree1c -> [Number]
dieptes n (Leaf1c) = [n]
dieptes n (Node1c _ t1 t2) = (dieptes (n+1) t1) ++ (dieptes (n+1) t2)

testbalans :: Tree1c -> Bool
testbalans tree = mx - mi <= 1
                where
                    dlijst = dieptes 0 tree
                    mx = maximum dlijst
                    mi = minimum dlijst

--b
makeBalans :: Tree1c -> Tree1c
makeBalans tree = makeBalans' (makeList tree)

makeBalans' [x] = Node1c x (Leaf1c) (Leaf1c)
makeBalans' [x,y] = Node1c x (Node1c y (Leaf1c) (Leaf1c)) (Leaf1c)
makeBalans' list = Node1c ((slist !! 1) FPPrac.!! 0) (makeBalans' (slist !! 0)) (makeBalans' (slist !! 2))
                 where
                     lengthlist = FPPrac.length list
                     slist = splitPlaces [lengthlist `div` 2,1,lengthlist] list






