import FPPrac
import FPPrac.Graphs
import Data.Maybe
import Data.Char (isDigit)
import Data.List ((\\), delete, partition, minimumBy)
import Prelude

-- | Store datatype
--   Hiermee worden alle variabelen van het
--   programmer gedefinieerd
data Store = Store 
             { pressedN :: Bool
             , pressedR :: Bool
             , pressedE :: Bool
             , pressedD :: Bool
             , pressedW :: Bool
             , pressedF :: Bool
             , pressedC :: Bool
             , pressedB :: Bool
             , pressedV :: Bool
             , pressedM :: Bool
             , node1Select :: Maybe Node
             , node2Select :: Maybe Node
             , graph :: Graph
             }                

             
-- | Begingraph
--   Dit is de begintoestand van de graaf      
nodeA = ('a', (100,100), Black)
nodeB = ('b', (200, 200), Black)
beginGraph = Graph [nodeA,nodeB, ('c', (200, 100), Black), ('d', (100, 200), Black)] [('a', 'b', Black, 5),('d','b',Black,1),('c','b',Black,2),('a','c',Black,3),('a','d',Black,6)] Directed Weighted

-- | BeginStore
--   Dit is de begintoestand van de store
beginStore = Store  { pressedN = False
                    , pressedR = False
                    , pressedE = False
                    , pressedD = False
                    , pressedW = False
                    , pressedF = False
                    , pressedC = False
                    , pressedB = False
                    , pressedV = False
                    , pressedM = False
                    , node1Select = Nothing
                    , node2Select = Nothing
                    , graph   = beginGraph
                    }
 
-- | Instructions
--   Dit is een lijst van alle mogelijke 
--   instructies 
instructions = Instructions [ "Instructions",
                              "Press 'n' and click on the screen to create a new node",
                              "Press 'r', click on a node and press a letter to rename the node",
                              "Press 'e', click on two nodes to create an edge",
                              "Press 'd', click on a node to delete the node",
                              "Press 'w', click on two nodes and press a number to weight the edge in between",
                              "Press 'f', click on two nodes to delete an edge",
                              "Press 'c'  click on a node to color the node red",
                              "press 'b'  to coler adjecent noded blue",
                              "press 'x' to color all subgraphs",
                              "press 'v' click on two nodes colors all paths",
                              "press 'm' click on two nodes to color the shortest getPaths",
                              "press 'z' to make all nodes black"
                            ]                             

-- | Resetcommands
--   Deze functie reset alle mogelijke commando variabelen van de store                            
resetCommands :: Store -> Store
resetCommands s = s { pressedN = False
                    , pressedR = False
                    , pressedE = False
                    , pressedD = False
                    , pressedW = False
                    , pressedF = False
                    , pressedC = False
                    , pressedB = False
                    , pressedV = False
                    , pressedM = False
                    , node1Select = Nothing
                    , node2Select = Nothing
                    }

-- | Start
--   Deze functie start het programma
--   De functie preEventloop start de server
--   en neemt de functie eventloop mee als handler
--   Ook wordt de begintoestand van de store meegegeven                    
start = preEventloop eventloop beginStore
                
-- | AddNode
--  Deze functie voegt de node toe aan de graaf
addNode :: Node -> Graph -> Graph
addNode n g@(Graph{nodes=ns}) = g {nodes=(n:ns)}

-- | AddEdge
-- Deze functie voegt de edge toe aan de graaf
addEdge :: Edge -> Graph -> Graph
addEdge e g@(Graph{edges=es}) = g {edges=(e:es)}

-- | Geeft de nodes terug die horen bij de edge
findNodesByEdge :: Edge -> Graph -> (Node, Node)
findNodesByEdge (l1, l2, _, _) graph = (n1, n2)
                                    where
                                        Just n1 = findNode l1 graph
                                        Just n2 = findNode l2 graph

-- | Geeft de node terug horend bij het label
findNode :: Label -> Graph -> Maybe Node
findNode l (Graph {nodes=nodes}) | null n = Nothing
                                 | otherwise = Just (head n)
                                where
                                    n = filter (\(nl, _, _) -> l == nl) nodes

-- | Geeft de edge terug horend bij de twee labels
findEdge :: Label -> Label -> Graph -> Maybe Edge
findEdge l1 l2 (Graph {edges=es}) | null e    = Nothing
                                  | otherwise = Just (head e)
                                  where
                                    e = filter (\(el1, el2, _, _) -> l1 == el1 && l2 == el2) es

-- | Geeft alle edges terug die van of naar de node gaan
--   die bijbehorend label heeft                                    
findEdgesSingleLabel :: Label -> Graph -> [Edge]
findEdgesSingleLabel l (Graph{edges=es}) = filter (\(el1, el2, _, _) -> el1 == l || el2 == l) es

-- | Verwijdert de node horend bij het label uit de graaf
removeNode :: Label -> Graph -> Graph
removeNode l g@(Graph{nodes=ns}) | nM == Nothing = g 
                                 | otherwise     = g {nodes = ns'}
                                where
                                    nM = findNode l g
                                    n  = fromJust nM
                                    ns' = delete n ns

-- | Verwijdert de edge horend bij de twee labels uit
--   de graaf
removeEdge :: Label -> Label -> Graph -> Graph
removeEdge l1 l2 g@(Graph{edges=es}) | eM == Nothing = g 
                                     | otherwise     = g {edges = es'}
                                    where
                                        eM  = findEdge l1 l2 g
                                        e   = fromJust eM
                                        es' = delete e es

-- | Verwijdert alle edges die van of naar de node gaan
--   horend bij het gegeven label
removeEdgesSingleLabel :: Label -> Graph -> Graph
removeEdgesSingleLabel l g@(Graph{edges=es}) = g {edges = es'}
                            where
                                es' = es \\ (findEdgesSingleLabel l g)

-- | Geeft de grafische output om de node te maken
nodeToOutput :: Node -> GraphOutput
nodeToOutput (l, p, c) = NodeG l p c


-- | Geeft de grafische output om de edge te maken
--  Deze functie houdt rekening met de eigenschappen
--  van de graaf zoals gewicht en directie
edgeToOutput :: Graph -> Edge -> GraphOutput
edgeToOutput graph@(Graph {weighted=weighted, directed=directed}) e@(l1, l2, c, w) | weighted == Weighted  = WeightedLineG n1 n2 w c Thin directed
                                                                                   | otherwise             = LineG n1 n2 c Thin directed 
                                                                                   where
                                                                                       (n1, n2) = findNodesByEdge e graph

-- | De Eventloop
--   Dit is het hart van het grafische IO programma.
--   Elk input dat van belang is wordt gemapt naar
--   de bijbehorende uitvoer.
eventloop :: Store -> GraphInput -> ([GraphOutput], Store)

--  Deze functie wordt aangeroepen bij het starten van het programma.
eventloop s@(Store {graph = g}) Start = (graphToOutput g, s)

-- | Deze functie hernoemt een node wanneer alle 
--   benodigde variabelen zijn gezet
eventloop s@(Store { pressedR = True
                   , node1Select = Just n
                   , graph = g
                   }) (KeyPress l) = (output, s'{graph=graph'})
                                  where
                                      (output, graph') = renameNode (head l) n g
                                      s'               = resetCommands s

-- | Deze functie geeft een gewicht aan een edge
--   wanneer alle benodigde variabelen zijn gezet
eventloop s@(Store { pressedW    = True
                   , node1Select = Just n1
                   , node2Select = Just n2
                   , graph       = g
                   }) (KeyPress d) | isDigit (head d) = (output, s'{graph=g'})
                                   | otherwise        = ([], s)
                                   where
                                        s' = resetCommands s
                                        i  = read d
                                        (output, g') = weightEdge n1 n2 i g
                            

-- | Zet het bijbehorende commando bij de toetsaanslag                            
eventloop s (KeyPress "n") = ([], s' {pressedN = True})
                      where
                          s' = resetCommands s
                          
-- | Zet het bijbehorende commando bij de toetsaanslag                          
eventloop s (KeyPress "r") = ([], s' {pressedR = True})
                      where
                          s' = resetCommands s
                          
-- | Zet het bijbehorende commando bij de toetsaanslag                          
eventloop s (KeyPress "e") = ([], s' {pressedE = True})
                      where
                          s' = resetCommands s
                          
-- | Zet het bijbehorende commando bij de toetsaanslag                          
eventloop s (KeyPress "d") = ([], s' {pressedD = True})
                      where
                          s' = resetCommands s
                          
-- | Zet het bijbehorende commando bij de toetsaanslag                          
eventloop s (KeyPress "w") = ([], s' {pressedW = True})
                      where
                          s' = resetCommands s
                          
-- | Zet het bijbehorende commando bij de toetsaanslag                          
eventloop s (KeyPress "f") = ([], s' {pressedF = True})
                      where
                          s' = resetCommands s                                          

-- | Zet het bijbehorende commando bij de toetsaanslag                          
eventloop s (KeyPress "c") = ([], s' {pressedC = True})
                      where
                          s' = resetCommands s

eventloop s (KeyPress "b") = ([], s' {pressedB = True})
                      where
                          s' = resetCommands s

eventloop s@(Store {graph = g}) (KeyPress "x") = (go,s' {graph = g'})
                      where
                            (go,g') = colorNodes g $ getSamenhangend g
                            s' = resetCommands s

eventloop s@(Store {graph = g@(Graph {nodes=ns})}) (KeyPress "z") = (go,s' {graph = g'})
                    where
                        (go,g') = changeColor Black g ns
                        s' = resetCommands s

eventloop s (KeyPress "v") = ([], s' {pressedV = True})
                      where
                          s' = resetCommands s

eventloop s (KeyPress "m") = ([], s' {pressedM = True})
                      where
                          s' = resetCommands s

-- | Deze functie geeft het correcte gedrag bij een muisklik.
--   Afhankelijk van welke toetsaanslag al is ingedrukt en of er
--   op een nodige is geklikt, wordt een variabele gezet of de complete
--   Store gezet met resetCommands.
eventloop s@(Store pn pr pe pd pw pf pc pb pv pm n1s n2s g) (MouseUp MLeft pos) | pn && node == Nothing                 = (output1, s' {graph=graph1})
                                                                        | pd && node /= Nothing                    = (output2, s' {graph=graph2})
                                                                        | pr && n1s  == Nothing                    = ([], s{node1Select = node})
                                                                        | pe && n1s  == Nothing                    = ([], s{node1Select = node})
                                                                        | pe && n1s  /= Nothing && node /= Nothing = (output3, s' {graph=graph3})
                                                                        | pw && n1s == Nothing                     = ([], s{node1Select = node})
                                                                        | pw && n1s /= Nothing                     = ([], s{node2Select = node})
                                                                        | pf && n1s == Nothing                     = ([], s{node1Select = node})
                                                                        | pf && n1s /= Nothing                     = (output4, s' {graph=graph4})
                                                                        | pc && node /= Nothing                    = (output5, s' {graph=graph5})
                                                                        | pb && node /= Nothing                    = (output6, s' {graph=graph6})
                                                                        | pv && n1s  == Nothing                    = ([], s{node1Select = node})
                                                                        | pv && n1s  /= Nothing && node /= Nothing = (output7, s' {graph=graph7})
                                                                        | pm && n1s  == Nothing                    = ([], s{node1Select = node})
                                                                        | pm && n1s  /= Nothing && node /= Nothing = (output8, s' {graph=graph8})
                                                                        | otherwise                                = ([], s)
                                                                              where
                                                                                (output1, graph1) = insertNode pos g
                                                                                (output2, graph2) = deleteNode (fromJust node) g
                                                                                (output3, graph3) = insertEdge (fromJust n1s) (fromJust node) g
                                                                                (output4, graph4) = deleteEdge (fromJust n1s) (fromJust node) g
                                                                                (output5, graph5) = makeNodeRed (fromJust node) g
                                                                                (output6, graph6) = makeNodesBlue (fromJust node) g
                                                                                (output7, graph7) = colorNodes g $ getPaths g (fromJust n1s) (fromJust node)
                                                                                (output8, graph8) = changeColor Green g $ getShortestPath g (fromJust n1s) (fromJust node)
                                                                                node              = onNode (nodes g) pos
                                                                                s' = resetCommands s

-- | Voor alle andere uitvoer hoeft er niks te gebeuren                                                                                
eventloop s _ = ([], s)  

-- | Deze functie insert een node in de graaf op de bijbehorende
--   positie en geeft het een lege label. Ook wordt de grafische
--   uitvoer gegeven.
insertNode :: Pos -> Graph -> ([GraphOutput], Graph)
insertNode pos g@(Graph {nodes=ns}) | nM == Nothing = (output, g')
                                    | otherwise     = ([], g)
                                    where
                                        l = ' '
                                        nM = findNode l g
                                        n = (l, pos, Black)
                                        g' = addNode n g
                                        output = [RemoveNodeG l, nodeToOutput n]

-- | Deze functie verwijdert een node uit de graaf. Ook wordt de
--   grafische uitvoer gegeven.
deleteNode :: Node -> Graph -> ([GraphOutput], Graph)
deleteNode (l, _, _) g = (output, g'')
                    where
                        g' = removeNode l g
                        edgesFromNode = findEdgesSingleLabel l g'
                        g'' = removeEdgesSingleLabel l g'
                        output = RemoveNodeG l:(map (\(el1, el2, _, _)-> RemoveEdgeG el1 el2) edgesFromNode)

-- | Deze functie insert een edge in de graaf.
--   Ook wordt de grafische uitvoer gegeven.
insertEdge :: Node -> Node -> Graph -> ([GraphOutput], Graph)
insertEdge n1@(l1,_,_) n2@(l2,_,_) g@(Graph {directed=d}) | eM == Nothing = ([output], g')
                                                          | otherwise     = ([], g)
                                                          where
                                                              eM = findEdge l1 l2 g
                                                              e = (l1, l2, Black, -1)
                                                              g' = addEdge e g
                                                              output = LineG n1 n2 Black Thin d

-- | Hernoemt een node en update alle bijbehorende edges
--   Geeft ook de bijbehorende grafische uitvoer
renameNode :: Label -> Node -> Graph -> ([GraphOutput], Graph)
renameNode l' (l, p, c) g = (outN ++ outEs, g''')
                        where
                            g'           = removeNode l g
                            n'           = (l', p, c)
                            g''          = addNode n' g'
                            outN         = [RemoveNodeG l, nodeToOutput n']
                            (outEs, g''') = renameEdges l l' g''

-- | Hernoemt alle edges horend tot de oldL. Alle oldL wordt
--   vervangen door l. Ook geeft deze functie de bijbehorende
--   grafische uitvoer.
renameEdges :: Label -> Label -> Graph -> ([GraphOutput], Graph)
renameEdges oldL l g = (output, g')
                    where
                        es = findEdgesSingleLabel oldL g
                        (output, g') = foldl renEs ([],g) es
                        renEs (o, graph) e = let
                                              (o', graph') = renE e graph
                                              in (o ++ o', graph')
                        renE e graph = renameEdge oldL l e graph
 

-- | Hernoemt een enkele edge 
renameEdge :: Label -> Label -> Edge -> Graph -> ([GraphOutput], Graph)
renameEdge oldL l e@(el1, el2, c, w) g@(Graph{weighted=wei, directed = d}) | el1 == oldL && el2 == oldL = ([rem, edgeToOutput g' e1], addEdge e1 g')
                                                                           | el1 == oldL                = ([rem, edgeToOutput g' e2], addEdge e2 g')
                                                                           | el2 == oldL                = ([rem, edgeToOutput g' e3], addEdge e3 g')
                                                                    where
                                                                        (n1, n2)  = findNodesByEdge e g
                                                                        rem       = RemoveEdgeG el1 el2
                                                                        g'        = removeEdge el1 el2 g
                                                                        e1        = (l, l, c, w)
                                                                        e2        = (l, el2, c, w)
                                                                        e3        = (el1, l, c, w)                               

makeNodeRed :: Node -> Graph -> ([GraphOutput], Graph)
makeNodeRed (l, p, c) g = (outN, g'')
                        where
                            g'           = removeNode l g
                            n'           = (l, p, Red)
                            g''          = addNode n' g'
                            outN         = [RemoveNodeG l, nodeToOutput n']

makeNodesBlue :: Node -> Graph -> ([GraphOutput], Graph)
makeNodesBlue (l, _, _) g@(Graph {edges=es, directed=dir}) = changeColor Blue g $ catMaybes $ map (\x -> findNode x g) $ getAdjacent l dir es

getAdjacent :: Label -> Directed -> [Edge] -> [Label]
getAdjacent l _ [] = []
getAdjacent l dir ((l1,l2,c,w):xs)  | l == l1 = [l2] ++ rest
                                    | dir == Undirected && l == l2 = [l1] ++ rest
                                    | otherwise = rest
                                    where
                                        rest = getAdjacent l dir xs

changeColor :: ColorG -> Graph -> [Node] -> ([GraphOutput],Graph)
changeColor c g [] = ([],g)
changeColor c g ((l,p,_):xs) = (outN++go,g''')
                        where
                            (go,g')      = changeColor c g xs
                            g''          = removeNode l g'
                            n'           = (l, p, c)
                            g'''         = addNode n' g''
                            outN         = [RemoveNodeG l, nodeToOutput n']

-- | Verandert het gewicht van de edge van node 1 naar node 2.                           
weightEdge :: Node -> Node -> Int -> Graph -> ([GraphOutput], Graph)
weightEdge n1@(l1, _, _) n2@(l2, _, _) w g | eM == Nothing = ([], g)
                                           | eM /= Nothing = ([rem, edgeToOutput g'' e'], g'')
                                          where
                                             rem          = RemoveEdgeG l1 l2
                                             eM           = findEdge l1 l2 g
                                             (_, _, c, _) = fromJust eM
                                             e'           = (l1, l2, c, w)
                                             g'           = removeEdge l1 l2 g
                                             g''          = addEdge e' g'

-- | Verwijdert de edge van node 1 naar node 2.
deleteEdge :: Node -> Node -> Graph -> ([GraphOutput], Graph)
deleteEdge (l1, _, _) (l2, _, _) g = ([RemoveEdgeG l1 l2], g')
                                    where
                                        g' = removeEdge l1 l2 g

graphToOutput :: Graph -> [GraphOutput]
graphToOutput graph@(Graph {edges=es, nodes=ns}) = (map nodeToOutput ns) ++ (map (edgeToOutput graph) es)

isConnected :: Graph -> Bool
isConnected g@(Graph ns es Undirected _) = isConnected' g ns

isConnected' :: Graph -> [Node] -> Bool
isConnected' g [] = True
isConnected' g (x:xs) = all (\p -> (isJust $ findEdge (getLabel x) (getLabel p) g)) xs && isConnected' g xs

getLabel (l,_,_) = l

hasEdge :: Graph -> Label -> Label -> Bool
hasEdge g@(Graph {directed=Undirected}) l1 l2 = isJust (findEdge l1 l2 g) && isJust (findEdge l2 l1 g)
hasEdge g@(Graph {directed=Directed}) l1 l2 = isJust $ findEdge l1 l2 g

isSamenhangend :: Graph -> Bool
isSamenhangend g@(Graph {nodes=[]}) = False
isSamenhangend g@(Graph {nodes=ns, edges=es}) = isSamenhangend' g [(head ns)] (tail ns)

isSamenhangend' :: Graph -> [Node] -> [Node] -> Bool
isSamenhangend' g [] left = left == []
isSamenhangend' g (x:xs) left = isSamenhangend' g (xs++reach) left'
                where
                    (reach,left') = partition (\p -> hasEdge g (getLabel x) (getLabel p)) left

getSamenhangend :: Graph -> [[Node]]
getSamenhangend g@(Graph {nodes=ns}) = getSamenhangend' g ns

getSamenhangend' :: Graph -> [Node] -> [[Node]]
getSamenhangend' g [] = []
getSamenhangend' g (x:xs) = [sub] ++ getSamenhangend' g rest
                            where
                                (sub, rest) = getSamenhangend'' g [x] xs

getSamenhangend'' :: Graph -> [Node] -> [Node] -> ([Node],[Node])
getSamenhangend'' g [] left = ([],left)
getSamenhangend'' g (x:xs) left = (sub++[x],rest)
                where
                    (reach,left')   = partition (\p -> hasEdge g (getLabel x) (getLabel p)) left
                    (sub, rest)     = getSamenhangend'' g (xs++reach) left'

colors = [Red, Blue, Green, Purple, Grey, Yellow, Orange, White]

colorNodes :: Graph -> [[Node]] -> ([GraphOutput],Graph)
colorNodes g [] = ([],g)
colorNodes g (x:xs) = (output'++output, g'')
                where
                    (output', g'') = colorNodes g' xs
                    (output, g') = changeColor ( colors Prelude.!! ((Prelude.length xs) `mod` (Prelude.length colors))) g x 

isReachable :: Graph -> Node -> Node -> Bool                  
isReachable g@(Graph {nodes=ns}) a b = isReachable' g b [a] ns'
            where
                ns' = delete a ns

isReachable' :: Graph -> Node -> [Node] -> [Node] -> Bool
isReachable' g b [] left = False
isReachable' g b (x:xs) left = x == b || isReachable' g b (xs++reach) left'
            where
                (reach,left')   = partition (\p -> hasEdge g (getLabel x) (getLabel p)) left

getPaths :: Graph -> Node -> Node -> [[Node]]
getPaths g@(Graph {nodes=ns}) a b = map (\l -> [a]++l) (getPaths' g a b (delete a ns))

getPaths' :: Graph -> Node -> Node -> [Node] -> [[Node]]
getPaths' g a b []      | a == b = [[]]
                        | otherwise = []
getPaths' g a b ns      | a == b = [[]]
                      	| otherwise = concatMap (\x -> map (\l -> [x]++l) (getPaths' g x b (delete x ns))) reach
     			    where
         				reach = filter (\p -> hasEdge g (getLabel a) (getLabel p)) ns

getShortestPath :: Graph -> Node -> Node -> [Node]
getShortestPath g a b = minimumBy (\x y -> compare (countWeight g x) (countWeight g y)) (getPaths g a b)

countWeight :: Graph -> [Node] -> Int
countWeight g [] = 0
countWeight g [x] = 0
countWeight g ((x,_,_):(y,y1,y2):ns) = (getWeight $ fromJust $ findEdge x y g) + countWeight g ((y,y1,y2):ns)

getWeight (_,_,_,w) = w


