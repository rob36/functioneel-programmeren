import FPPrac
import Data.Char
import Data.List
import Data.Ord
--import Prelude

-- opgave 1
myfilter :: (a -> Bool) -> [a] -> [a]
myfilter f [] = []
myfilter f (x:xs)	| f x = x : myfilter f xs
			| otherwise = myfilter f xs

myfoldl :: (a -> b -> a) -> a -> [b] -> a
myfoldl f a [] = a
myfoldl f a [x] = f a x
myfoldl f a (x:xs) = myfoldl f (f a x) xs   

myfoldr :: (a -> b -> b) -> b -> [a] -> b
myfoldr f a [] = a
myfoldr f a [x] = f x a
myfoldr f a (x:xs) = f  x (myfoldr f a xs)

myzipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
myzipWith f [] _ = []
myzipWith f _ [] = []
myzipWith f (x:xs) (y:ys) = f x y : myzipWith f xs ys

--opgave 2
-- a
type Person = (String, Number, Bool, String)
personen = [("Henk", 118, True, "Hengelo"),("Anja", 30, False, "Enschede"),("Fritsche", 45, True,"Bocholt")]
--krijgPersoon :: String -> [Person] -> Maybe Person
--krijgPersoon a [] = Nothing
--krijgPersoon a (x:xs)	| zelfdeNaam a x = Just x
--			| otherwise = krijgPersoon a xs
 
-- b
krijgPersoon :: String -> [Person] -> Person
krijgPersoon a [] = error "Person not found"
krijgPersoon a (x:xs)	| zelfdeNaam a x = x
			| otherwise = krijgPersoon a xs

zelfdeNaam :: String -> Person -> Bool
zelfdeNaam a (naam,_,_,_) = (map toLower a) == (map toLower naam)

krijgNaam (naam,_,_,_) = naam
krijgLeeftijd (_,leeftijd,_,_) = leeftijd
krijgGeslacht (_,_,geslacht,_) = geslacht
krijgWoonplaats (_,_,_,plaats) = plaats

-- c
hoogop :: Number -> [Person] -> [Person]
hoogop n [] = []
hoogop n ((naam,leeftijd,geslacht,plaats):db) = (naam,leeftijd+n,geslacht,plaats) : hoogop n db

hoogop' :: Number -> [Person] -> [Person]
hoogop' n db = [ (naam,leeftijd+n,geslacht,plaats) | (naam,leeftijd,geslacht,plaats) <- db]

hoogop'' :: Number -> [Person] -> [Person]
hoogop'' n db = map (hoogoppersoon n) db 

hoogoppersoon :: Number -> Person -> Person
hoogoppersoon n (naam,leeftijd,geslacht,plaats) = (naam,leeftijd+n,geslacht,plaats) 

--d
zoekmilf :: [Person] -> [Person]
zoekmilf [] = []
zoekmilf ((naam,leeftijd,geslacht,plaats):db) | leeftijd >= 30 && leeftijd <=40 &&  (not)geslacht = (naam,leeftijd,geslacht,plaats) : zoekmilf db
                                              | otherwise = zoekmilf db
zoekmilf' :: [Person] -> [Person]
zoekmilf' db = [(naam,leeftijd,geslacht,plaats) | (naam,leeftijd,geslacht,plaats) <- db, leeftijd >= 30 && leeftijd <=40 &&  (not)geslacht]

zoekmilf'' :: [Person] -> [Person]
zoekmilf'' db = filter (\(naam,leeftijd,geslacht,plaats) -> leeftijd >= 30 && leeftijd <=40 &&  (not)geslacht) db

--e
krijgpersoonsleeftijd :: String -> [Person] -> Number
krijgpersoonsleeftijd naam db = krijgLeeftijd (krijgPersoon naam db)

--f
sortdbleeftijd :: [Person] -> [Person]
sortdbleeftijd db =  sortBy (\(_,leeftijd1,_,_) (_,leeftijd2,_,_) -> compare leeftijd1 leeftijd2) db

-- opgave 3
-- a
deelbaar :: Number -> Number -> Bool
deelbaar a b = b `mod` a /= 0

zeef :: [Number]
zeef = zeef' [2..]
zeef' :: [Number] -> [Number]
zeef' (x:xs) = x : zeef' (filter (deelbaar x) xs)

ispriem :: Number -> Bool
ispriem n = ispriem' n zeef
ispriem' :: Number -> [Number] -> Bool
ispriem' n (x:xs) | x == n = True
		  | x > n = False
		  | otherwise = ispriem' n xs

neempriems :: Int -> [Number]
neempriems n = Data.List.take n zeef

neemkleinerdan :: Number -> [Number]
neemkleinerdan n = neemkleinerdan' n zeef
neemkleinerdan' n (x:xs) | x == n = [x]
			 | x > n = []
			 | otherwise = x : neemkleinerdan' n xs

-- b
delers :: Number -> [Number]
delers n = [x | x <- [2.. floor $ sqrt n], n `mod` x == 0 ]

geendelers :: Number -> Bool
geendelers n = [] == (delers n)

-- opgave 4

pyth :: Number -> [(Number,Number,Number)]
pyth n = [(a,b,c) | a <- [1..n], b <- [1..n],c <- [1..n], a^2 + b^2 == c^2]
pyth' :: Number -> [(Number,Number,Number)]
pyth' n = [(a,b,c) | a <- [1..n], b <- [1..n],c <- [1..n], a^2 + b^2 == c^2, a<=b, (((delers a) `intersect` (delers b)) `intersect` (delers c)) == []]

--opgave 5
--a
oplopend :: [Number] -> Bool
oplopend [x] = True
oplopend (x:y:xs) = x<y && oplopend (y:xs)
--b
zwakstijgend :: [Number] -> Bool
zwakstijgend a = zwakstijgend' 0 0 a
zwakstijgend' :: Number -> Number -> [Number] -> Bool
zwakstijgend' g a [x] = g<x
zwakstijgend' g a (x:xs) = g<x && (zwakstijgend' ((g*a+x)/(a+1)) (a+1) xs)

--opgave 6
--a
deellijst :: [Number] -> [Number] -> Bool
deellijst [] [] = True
deellijst _ [] = False
deellijst [] _ = True
deellijst (x:xs) (y:ys) = ( x==y && deellijst' xs ys ) || deellijst (x:xs) ys

deellijst' :: [Number] -> [Number] -> Bool
deellijst' [] [] = True
deellijst' _ [] = False
deellijst' [] _ = True
deellijst' (x:xs) (y:ys) = x == y && deellijst xs ys

--b
sublijst :: [Number] -> [Number] -> Bool
sublijst [] [] = True
sublijst _ [] = False
sublijst [] _ = True
sublijst (x:xs) (y:ys) | x==y = deellijst xs ys
                       | otherwise = deellijst (x:xs) ys

--opgave 7
bubblesort :: [Number] -> [Number]
bubblesort [] = []
bubblesort xs | i == 0 = bxs
              | otherwise = bubblesort bxs
               where
                    (i,bxs) = bubblesort' xs 0

bubblesort' :: [Number] -> Number -> (Number,[Number])
bubblesort' [] i = (i,[])
bubblesort' [x] i = (i,[x])
bubblesort' (x:y:xs) i | x>y = bubblesort'' y (x:xs) (i+1)
                       | otherwise = bubblesort'' x (y:xs) i

bubblesort'' :: Number -> [Number] -> Number -> (Number,[Number])
bubblesort'' m [] i = (i,[m])
bubblesort'' m xs i = (j,m:bxs)
                    where
                         (j,bxs) = bubblesort' xs i

--opgave 8
minmax :: [Number] -> [Number]
minmax [] = []
minmax [x] = [x]
minmax xs = mi : minmax (xs \\ [mi,ma]) ++ [ma]
            where 
              mi = minimum xs
              ma = maximum xs

--opgave 9
isort :: [Number] -> [Number]
isort x = foldr insert [] x

-- opgave 10
msort :: [Number] -> [Number]
msort [] = []
msort [x] = [x]
msort xs = merge (msort half1) (msort half2)
         where
               (half1,half2) = Data.List.splitAt (Data.List.length xs `div` 2) xs

merge :: [Number] -> [Number] -> [Number]
merge [] xs = xs
merge xs [] = xs
merge (x:xs) (y:ys) | x < y = x : merge xs (y:ys)
                    | otherwise = y : merge (x:xs) ys

-- opgave 11
qsort :: [Number] -> [Number]
qsort [] = []
qsort [x] = [x]
qsort (x:xs) = (qsort (filter (<x) xs)) ++ [x] ++ (qsort (filter (==x) xs)) ++ (qsort (filter (>x) xs))







